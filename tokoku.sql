-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 192.168.23.207:3306
-- Generation Time: 13 Sep 2019 pada 23.56
-- Versi Server: 10.0.33-MariaDB
-- PHP Version: 5.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tokoku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_version`
--

CREATE TABLE IF NOT EXISTS `app_version` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version_code` int(11) NOT NULL,
  `version_name` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `app_version`
--

INSERT INTO `app_version` (`id`, `version_code`, `version_name`, `active`, `created_at`, `last_update`) VALUES
(1, 1, '1', 1, 1484386275760, 1485795959275),
(2, 2, '2', 1, 1485795799112, 1485795799112),
(3, 3, '3', 1, 1557291333392, 1557291333392);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bank`
--

CREATE TABLE IF NOT EXISTS `bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bank` varchar(20) NOT NULL,
  `img_bank` text,
  `norek` varchar(30) NOT NULL,
  `atas_nama` varchar(50) NOT NULL,
  `draft` int(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `bank`
--

INSERT INTO `bank` (`id`, `nama_bank`, `img_bank`, `norek`, `atas_nama`, `draft`, `created_at`) VALUES
(1, 'BNI', 'BNI.png', '2387468274628', 'COBA UPDATE', 0, '2019-03-18 10:30:56'),
(2, 'BCA', 'bca.png', '03083973893', 'Bagus', 0, '2019-03-18 11:58:10'),
(5, 'BRI', '1552992714129.png', '873783782929', 'Budi Coba', 0, '2019-03-19 10:51:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `draft` tinyint(1) NOT NULL,
  `brief` varchar(100) NOT NULL,
  `color` varchar(7) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`id`, `name`, `icon`, `draft`, `brief`, `color`, `priority`, `created_at`, `last_update`) VALUES
(3, 'Makanan dan Minuman Ringan', 'Makanan dan Minuman Ringan.png', 0, 'Snack, SoftDrink, Mie, Kopi Instan, Minuman, dll', '#FA897B', 10, 1485609656784, 1556262281393),
(4, 'Sembako', 'Sembako.png', 0, 'Beras, Minyak Goreng, Telur, Bumbu Masak, Gas, Air Mineral, Gula, Kopi, Tepung, dll', '#D0E6A5', 1, 1485609722495, 1556262226992),
(5, 'Perangkat Mandi dan Cuci', 'Perangkat Mandi dan Cuci.png', 0, 'Sabun, Detergen, Pasta Gigi, Pembersih Lantai Kaca, Sikat Gigi, dll', '#8474A1', 3, 1516759844056, 1556262101953),
(6, 'Kebutuhan Bayi dan Anak', 'Kebutuhan Bayi dan Anak.png', 0, 'Baju Anak, Popok, Susu, Dot, Mainan, Kereta Anak, Bantal Guling, dll', '#FF8357', 4, 1516761373534, 1556262393967),
(7, 'Rumah Tangga', 'Rumah Tangga.png', 0, 'Mebelair, Kelambu, Pengharum Ruangan, Aksesories Rumah, dll', '#08979D', 11, 1516761730333, 1556262174766),
(8, 'Makanan Minuman Siap Saji', 'Makanan Minuman Siap Saji.png', 0, 'Ayam, Sate, Seafood, Juice, Bakso, Mie Ayam, Martabak, dll', '#6EC6CA', 6, 1516762531628, 1556262137476),
(9, 'Grosir', 'Grosir.png', 0, 'Barang Grosir Partai Besar', '#055B5C', 10, 1516763013542, 1556262200269),
(10, 'Elektronik', 'Elektronik.png', 0, 'TV, AC, Lemari Es, Kipas Angin, Sound System, Dispenser, dll', '#DCD964', 9, 1516763374391, 1556262324004),
(11, 'Mode dan Gaya', 'Mode dan Gaya.png', 0, 'Baju, Tas, Dompet, Sepatu, Jaket, Topi, Kacamata, dll', '#CCABD8', 8, 1516763994659, 1556262429760),
(12, 'Jasa dan Layanan', 'Jasa dan Layanan.png', 0, 'Service, Ojek, Tiket, Pulsa, Pijat, Kurir, dll', '#e27171', 8, 1516764556143, 1555323473765),
(13, 'Lain Lain', 'Lain Lain.png', 0, 'Rokok, Materai dan lain lain', '#982929', 12, 1517245037874, 1555414193924),
(14, 'Pasar Kita', 'Pasar Kita.png', 0, 'Sayuran, Ikan, Daging, Buah, dll (buka jam 08.00 - 14.00)', '#7EA00E', 2, 1519732534791, 1556262368465);

-- --------------------------------------------------------

--
-- Struktur dari tabel `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `code` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `config`
--

INSERT INTO `config` (`code`, `value`) VALUES
('CURRENCY', 'Rp'),
('EMAIL_BCC_RECEIVER', '["tokoku@dokterapps.com"]'),
('EMAIL_NOTIF_ON_ORDER', 'TRUE'),
('EMAIL_NOTIF_ON_ORDER_PROCESS', 'TRUE'),
('EMAIL_REPLY_TO', 'tokoku@dokterapps.com'),
('EMAIL_SENDER', 'tokoku@dokterapps.com'),
('EMAIL_SENDER_PASS', 'xxxxxx'),
('FEATURED_NEWS', '5'),
('SHIPPING', '["POS","GOJEK","JNE","SICEPAT","JNT"]'),
('TAX', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=165 ;

--
-- Dumping data untuk tabel `currency`
--

INSERT INTO `currency` (`id`, `code`, `name`) VALUES
(1, 'AFA', 'Afghanistan afghani'),
(2, 'ALL', 'Albanian lek'),
(3, 'DZD', 'Algerian dinar'),
(4, 'AOR', 'Angolan kwanza reajustado'),
(5, 'ARS', 'Argentine peso'),
(6, 'AMD', 'Armenian dram'),
(7, 'AWG', 'Aruban guilder'),
(8, 'AUD', 'Australian dollar'),
(9, 'AZN', 'Azerbaijanian new manat'),
(10, 'BSD', 'Bahamian dollar'),
(11, 'BHD', 'Bahraini dinar'),
(12, 'BDT', 'Bangladeshi taka'),
(13, 'BBD', 'Barbados dollar'),
(14, 'BYN', 'Belarusian ruble'),
(15, 'BZD', 'Belize dollar'),
(16, 'BMD', 'Bermudian dollar'),
(17, 'BTN', 'Bhutan ngultrum'),
(18, 'BOB', 'Bolivian boliviano'),
(19, 'BWP', 'Botswana pula'),
(20, 'BRL', 'Brazilian real'),
(21, 'GBP', 'British pound'),
(22, 'BND', 'Brunei dollar'),
(23, 'BGN', 'Bulgarian lev'),
(24, 'BIF', 'Burundi franc'),
(25, 'KHR', 'Cambodian riel'),
(26, 'CAD', 'Canadian dollar'),
(27, 'CVE', 'Cape Verde escudo'),
(28, 'KYD', 'Cayman Islands dollar'),
(29, 'XOF', 'CFA franc BCEAO'),
(30, 'XAF', 'CFA franc BEAC'),
(31, 'XPF', 'CFP franc'),
(32, 'CLP', 'Chilean peso'),
(33, 'CNY', 'Chinese yuan renminbi'),
(34, 'COP', 'Colombian peso'),
(35, 'KMF', 'Comoros franc'),
(36, 'CDF', 'Congolese franc'),
(37, 'CRC', 'Costa Rican colon'),
(38, 'HRK', 'Croatian kuna'),
(39, 'CUP', 'Cuban peso'),
(40, 'CZK', 'Czech koruna'),
(41, 'DKK', 'Danish krone'),
(42, 'DJF', 'Djibouti franc'),
(43, 'DOP', 'Dominican peso'),
(44, 'XCD', 'East Caribbean dollar'),
(45, 'EGP', 'Egyptian pound'),
(46, 'SVC', 'El Salvador colon'),
(47, 'ERN', 'Eritrean nakfa'),
(48, 'EEK', 'Estonian kroon'),
(49, 'ETB', 'Ethiopian birr'),
(50, 'EUR', 'EU euro'),
(51, 'FKP', 'Falkland Islands pound'),
(52, 'FJD', 'Fiji dollar'),
(53, 'GMD', 'Gambian dalasi'),
(54, 'GEL', 'Georgian lari'),
(55, 'GHS', 'Ghanaian new cedi'),
(56, 'GIP', 'Gibraltar pound'),
(57, 'XAU', 'Gold (ounce)'),
(58, 'XFO', 'Gold franc'),
(59, 'GTQ', 'Guatemalan quetzal'),
(60, 'GNF', 'Guinean franc'),
(61, 'GYD', 'Guyana dollar'),
(62, 'HTG', 'Haitian gourde'),
(63, 'HNL', 'Honduran lempira'),
(64, 'HKD', 'Hong Kong SAR dollar'),
(65, 'HUF', 'Hungarian forint'),
(66, 'ISK', 'Icelandic krona'),
(67, 'XDR', 'IMF special drawing right'),
(68, 'INR', 'Indian rupee'),
(69, 'Rp', 'Indonesian rupiah'),
(70, 'IRR', 'Iranian rial'),
(71, 'IQD', 'Iraqi dinar'),
(72, 'ILS', 'Israeli new shekel'),
(73, 'JMD', 'Jamaican dollar'),
(74, 'JPY', 'Japanese yen'),
(75, 'JOD', 'Jordanian dinar'),
(76, 'KZT', 'Kazakh tenge'),
(77, 'KES', 'Kenyan shilling'),
(78, 'KWD', 'Kuwaiti dinar'),
(79, 'KGS', 'Kyrgyz som'),
(80, 'LAK', 'Lao kip'),
(81, 'LVL', 'Latvian lats'),
(82, 'LBP', 'Lebanese pound'),
(83, 'LSL', 'Lesotho loti'),
(84, 'LRD', 'Liberian dollar'),
(85, 'LYD', 'Libyan dinar'),
(86, 'LTL', 'Lithuanian litas'),
(87, 'MOP', 'Macao SAR pataca'),
(88, 'MKD', 'Macedonian denar'),
(89, 'MGA', 'Malagasy ariary'),
(90, 'MWK', 'Malawi kwacha'),
(91, 'MYR', 'Malaysian ringgit'),
(92, 'MVR', 'Maldivian rufiyaa'),
(93, 'MRO', 'Mauritanian ouguiya'),
(94, 'MUR', 'Mauritius rupee'),
(95, 'MXN', 'Mexican peso'),
(96, 'MDL', 'Moldovan leu'),
(97, 'MNT', 'Mongolian tugrik'),
(98, 'MAD', 'Moroccan dirham'),
(99, 'MZN', 'Mozambique new metical'),
(100, 'MMK', 'Myanmar kyat'),
(101, 'NAD', 'Namibian dollar'),
(102, 'NPR', 'Nepalese rupee'),
(103, 'ANG', 'Netherlands Antillian guilder'),
(104, 'NZD', 'New Zealand dollar'),
(105, 'NIO', 'Nicaraguan cordoba oro'),
(106, 'NGN', 'Nigerian naira'),
(107, 'KPW', 'North Korean won'),
(108, 'NOK', 'Norwegian krone'),
(109, 'OMR', 'Omani rial'),
(110, 'PKR', 'Pakistani rupee'),
(111, 'XPD', 'Palladium (ounce)'),
(112, 'PAB', 'Panamanian balboa'),
(113, 'PGK', 'Papua New Guinea kina'),
(114, 'PYG', 'Paraguayan guarani'),
(115, 'PEN', 'Peruvian nuevo sol'),
(116, 'PHP', 'Philippine peso'),
(117, 'XPT', 'Platinum (ounce)'),
(118, 'PLN', 'Polish zloty'),
(119, 'QAR', 'Qatari rial'),
(120, 'RON', 'Romanian new leu'),
(121, 'RUB', 'Russian ruble'),
(122, 'RWF', 'Rwandan franc'),
(123, 'SHP', 'Saint Helena pound'),
(124, 'WST', 'Samoan tala'),
(125, 'STD', 'Sao Tome and Principe dobra'),
(126, 'SAR', 'Saudi riyal'),
(127, 'RSD', 'Serbian dinar'),
(128, 'SCR', 'Seychelles rupee'),
(129, 'SLL', 'Sierra Leone leone'),
(130, 'XAG', 'Silver (ounce)'),
(131, 'SGD', 'Singapore dollar'),
(132, 'SBD', 'Solomon Islands dollar'),
(133, 'SOS', 'Somali shilling'),
(134, 'ZAR', 'South African rand'),
(135, 'KRW', 'South Korean won'),
(136, 'LKR', 'Sri Lanka rupee'),
(137, 'SDG', 'Sudanese pound'),
(138, 'SRD', 'Suriname dollar'),
(139, 'SZL', 'Swaziland lilangeni'),
(140, 'SEK', 'Swedish krona'),
(141, 'CHF', 'Swiss franc'),
(142, 'SYP', 'Syrian pound'),
(143, 'TWD', 'Taiwan New dollar'),
(144, 'TJS', 'Tajik somoni'),
(145, 'TZS', 'Tanzanian shilling'),
(146, 'THB', 'Thai baht'),
(147, 'TOP', 'Tongan paanga'),
(148, 'TTD', 'Trinidad and Tobago dollar'),
(149, 'TND', 'Tunisian dinar'),
(150, 'TRY', 'Turkish lira'),
(151, 'TMT', 'Turkmen new manat'),
(152, 'AED', 'UAE dirham'),
(153, 'UGX', 'Uganda new shilling'),
(154, 'XFU', 'UIC franc'),
(155, 'UAH', 'Ukrainian hryvnia'),
(156, 'UYU', 'Uruguayan peso uruguayo'),
(157, 'USD', 'US dollar'),
(158, 'UZS', 'Uzbekistani sum'),
(159, 'VUV', 'Vanuatu vatu'),
(160, 'VEF', 'Venezuelan bolivar fuerte'),
(161, 'VND', 'Vietnamese dong'),
(162, 'YER', 'Yemeni rial'),
(163, 'ZMK', 'Zambian kwacha'),
(164, 'ZWL', 'Zimbabwe dollar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fcm`
--

CREATE TABLE IF NOT EXISTS `fcm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device` varchar(100) NOT NULL,
  `os_version` varchar(100) NOT NULL,
  `app_version` varchar(10) NOT NULL,
  `serial` varchar(100) NOT NULL,
  `regid` text NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=164 ;

--
-- Dumping data untuk tabel `fcm`
--

INSERT INTO `fcm` (`id`, `device`, `os_version`, `app_version`, `serial`, `regid`, `created_at`, `last_update`) VALUES
(4, 'Device Name', '6.0.1', '1.0', 'GGC00C0888E426A', 'APA91bEj7qmlVePXUpG4UjKOtyqG5x9hpeZ4tMhPDsJgDRWL76psPGtckLK3uMtmpLFj3RSFfgaVoBMCKhg5iR8RnPZPjeuml8Llgkc', 0, 0),
(6, 'HUAWEI ELE-L09', '7.1.2', '1 (1.1)', '34362251', 'fama3RyqBz0:APA91bFK9WETaobLBNS496F74V29LC76HE4p2gO5wHBBcSpJXKO5Tc5N5o0_EGdcrR4BL0uk9DDyOWf3RQvVpR6jONN5VUgZaYhzeMo8wJaZrZmGHazwzaG4so9FjTSfOgy_EShvqToz', 1552493957229, 1561538611089),
(7, 'LENOVO Lenovo A7010a48', '6.0', '1 (1.1)', 'AIH6CA9S7LNFYDHI', 'dt18c40Ox2A:APA91bG7OnaVOJNj_otl1VcTY8NSAWikQ7gjTpysmz9ISxDRbb3m6pPpjsuSow1M77kSWK2ITTtD0sJSQRqTA0Nz9CDd-4bSQ5sKg6YzI1R2Hn5VGA6SOwdCRfi-KzG_oY_TmuOnjA9b', 1552559703735, 1567740509640),
(8, 'vivo 1806', '9', '1 (1.1)', '6SIBZSRKQ465TSSS', 'e24Gl4wwJrY:APA91bHllRYGLS7EACyLxrOLsfxBjFYUVgDgt7A4tyAg3ydEIVcH-84o2Jj2Zqg63pAbUgNjzGZ1WmtVR8sl3a_BExJGhDAc2Y61_A__MfhFCtkj20XjnGzXkOx9Y9zd6R2w4d0YgqzY', 1552561042151, 1567064666848),
(9, 'Xiaomi Redmi Note 4', '7.0', '1 (1.0)', '358b74320704', 'euuQo2aoG5c:APA91bG-hwsn6KSsHT242X5l1GPf1olSISYTNqjtbgPzhSky1oz0oOytfkY-p_F5FLnY-CfFtsL8LjPa-R41r1-AJG8DcdqsLjGuD7pKGV_0T2S5Z3yMGbbd-jEm7-GCx8A5CNyMyJN8', 1552563162671, 1552563162671),
(10, 'Xiaomi MI NOTE LTE', '6.0.1', '1 (1.0)', '9589b8d4', 'cH_AGSx0Tcg:APA91bEXKxgKLKs62gZDYKDLat6xFnd8q8ju1A99nnm8cutt5fMzSuHhFNd9MWyIhJkCx8UiBx4r973TiHeiFivs4KmyVNIUgRcBFEGuoeRlLfkhWnP9dRx4O-45jZlzQs0st5O4lLM1', 1552563190337, 1552566378876),
(11, 'samsung SM-J730G', '9', '1 (1.1)', '52005e39ec84543b', 'dUKz1Z3LdPU:APA91bFzvijm9o7pCnt2z7ij_i2ha1zmSU3AbTAMH8nfyKj4kaJkvdpC9l7KHEjV-C69nhFxgh8qDCt737b7KXVEJJvxm-XwmCjSm4CcXWDrNHLWtZdu0MP4QsAAmHYOxAKRShn5EWCr', 1552567157474, 1568007647274),
(12, 'OPPO A37f', '5.1.1', '1 (1.0)', '23950371', 'fBydBmgdc6A:APA91bFBE8fA4QLWtuskLcdTB45gWGXteNDCk18WTN9_VKh7Wt3xi_sf4ktXZtWR3sMxd2z1X5dlvfl1gm8SUsd-Rjkf1k0HHqN3ppAmuHHy0wTmD2dd19bef3WczABtahdNzIUWeqsu', 1552576990355, 1552577646193),
(13, 'Xiaomi Redmi 5 Plus', '7.1.2', '1 (1.0)', '281108940105', 'eJ7_xEnrzFw:APA91bFcEz7L7L5tXmMCXvec_BBzzvzxudTijBNgovaU4UIT5rVByjVOGewiwDZyuizC_UJxxmGvtOPH6wNt5Ad8yO0LbcvVVhtTw80hAkirgZNgeoRfQOULnx5XVFGkgOQNhAcOsBX2', 1552728080644, 1553147529769),
(14, 'EVERCOSS_S50 S50', '7.0', '1 (1.1)', '0123456789ABCDEF', 'epWilYjdDGI:APA91bFfbAaq_Fx-0htGRv505wl7oWRIOCgoP8BqjIjnvswc89L2qiKeYPcsOJtizcjLmLGGdkm5vZNGtH7OSKJkBXMMpvQo2d9qWvm1v5P3d0cvC6-F2A_etXesstsiy0O2FdjeNuvE', 1552884402729, 1561523272395),
(15, 'Xiaomi Redmi S2', '8.1.0', '1 (1.0)', 'fbfb0a', 'e0-RuEG7vjM:APA91bFWOVTSzSPxiplvZtqK2dq4wkdH450Qk9GKNI-Zwtd9PySLs4xqdKfpevVv01oZRHGjFEjpPLaq4Ps-B4N87U0SM0gET7eCQFl1n3v7U5c1y275x1_y6IFHB5_WQSAN446lxMAx', 1552895422807, 1553149152157),
(16, 'OPPO RMX1805', '8.1.0', '1 (1.1)', '45489d92', 'eoUwQTBE6Aw:APA91bHWE26QR21vBCYmZjYLHJuAk8icGKnAQgbCMK3t0iaAJG3ewyPU6da0Om7-mm4Si8IggdAAbZBk7iEjMLVo-_U3I1wzWHMeI0K96232n6R_ytdq83FpLwatKzZDSN-CKvzwJG84', 1553002266412, 1556543335019),
(17, 'LGE Nexus 5X', '6.0.1', '1 (1.0)', '5645121a4e21595b', 'BLACKLISTED', 1553153307326, 1553153307326),
(18, 'unknown WindsongAOB', '6.0.1', '3 (1.3)', 'GCEX86', 'e7mhNWEN3Eo:APA91bEzvvIuYVwqu95Ho6gX-qkUbBHbcyBKqQ37eq6E_yKk3EK4IRk3iYXArTxancFrKnqgXhuJwcR351_lacw7j7wD1F_xu_7YAVu2LJPWnW017Ct_ggYMPAI-vAIj0re-S2luhSbk', 1553153369111, 1557785138682),
(19, 'LGE Nexus 5X', '6.0.1', '1 (1.1)', 'd9a1084ca54f8158', 'BLACKLISTED', 1553953005020, 1553953005020),
(20, 'Google Pixel 2', '9', '1 (1.1)', 'HT7BG1A03986', 'fYFPT0WBcx0:APA91bHC9f1oJMMaoeQZH_9y9WkXYy9a2dmKURnXABYfbY7qDZfbnvSC4dk-x7Ov5q7jXcrakgEOEmZYpjNkGjzxLy5NVcQNlumf3OnlGbVZyCoIlCIDuheS1T8sLWrKZWvAdSUYO-VT', 1553959364838, 1553959364838),
(21, 'Google Pixel 2', '8.1.0', '1 (1.1)', 'HT7A21A00069', 'fkYnf0J41Po:APA91bEbD-WLmcuOKyM4QeTesruNuQWl4rnQTf1dh6ITwxnbKIAmvENuT7q7UXekkUBfWidmmS1dpU1YtlLI2qL3VD483gR-CVP0UrBXNvGTlUmUwV6g0eh2lDXeOt-6ODA4bMGBSnxJ', 1553959414872, 1553959414872),
(22, 'motorola XT1650', '7.0', '1 (1.1)', 'ZY224LQJ23', 'dQkBOtNuUVc:APA91bEA4fOPZpLicxYtONA_Zs9ZGJklP66Gcn4GefEe4KFKcUUTK9TFoR0R2qRONYkgUa8tdjk5CUwYEVucw-dQ7VvXSxbm6bsbeQi0Jh8nFbKmfLPzwnL__BvOp2YEASaQxUPCWcXI', 1553959443462, 1553959443462),
(23, 'samsung GT-I9300', '4.3', '1 (1.1)', '4df71de30f2711ed', 'd99whhd4Ubo:APA91bEJtXJIkBhMkrHh4Mtp45mpbys1zYc9APhXSutMnoxpzXNw6LXj_dlFPnEsEARhXLOCB2Uc_RrCmeT_uf1kYf8r6mEUAHhdEgJh_QOVOfWkGajjjJ8ZM8LeftYj2s8dNToWgVXc', 1553959473893, 1553959473893),
(24, 'Sony G8441', '8.0.0', '1 (1.1)', 'BH906ECH9E', 'cyNkxLpsxno:APA91bHgFdNdkv6edcNvIVlrNUPicuZKUlYzMVtPmanagesBZNies3wIxD5qcGNRTFtU9Ynfw4ILMjuG9zIWM3uvNnKilB6JjUd0TRMQ-iFxkdqfIalVii9nQfFz-IGs6Jgw93mgc-fs', 1553959567473, 1553959567473),
(25, 'Sony D5833', '6.0.1', '1 (1.1)', 'YT9113FKWQ', 'dsRF2_FngBI:APA91bH3Z8kk0b8p0-wJHyS23C4JUr0Hra8fI0eOpkt7ib1i15t_d1mDepPySnNwsDMYcBIK3T55FTxCkwljIqkspMLFBggH5pHVVS9NkAeB0BH6ZveOW6S8osuLD2dG3flpFtI0JtrZ', 1554107770137, 1554107770137),
(26, 'Sony D5833', '6.0.1', '1 (1.1)', 'YT9113FKWQ', 'dsRF2_FngBI:APA91bH3Z8kk0b8p0-wJHyS23C4JUr0Hra8fI0eOpkt7ib1i15t_d1mDepPySnNwsDMYcBIK3T55FTxCkwljIqkspMLFBggH5pHVVS9NkAeB0BH6ZveOW6S8osuLD2dG3flpFtI0JtrZ', 1554107768359, 1554107768359),
(27, 'samsung SM-J320G', '5.1.1', '1 (1.1)', '4200fd50f20b7300', 'eQnHRj0QNcM:APA91bEruIzkIK_J97iaChIXQ794krgdzHEZxIJqj5LqTQbmE6YDBAuGWbR-YAQQz0lauw2yEojtd1uvHiv4GPk-osu-VCDUVEiecfxQYM7UYhl6TQBsj8gp9bZGUEzJy-Tu-3MEMYJQ', 1554229047680, 1554231170557),
(28, 'Xiaomi Redmi Pro', '6.0', '1 (1.1)', '95SGKJWOSGQKT45T', 'fK2N_kmxEPw:APA91bEQyZIqRimVEOyzC0Q1vVl9qii3_VN4Q6CYpA-fAwCqYnANIN7NiXRgfaolF3vZ5GqPzebsjbOUJMhgD3pc7zOvDceM_bK6we2ZQm4BwhCaKuCjVFt6M9hXZ-Rh016rNc3zaPZJ', 1554249859823, 1554249859823),
(29, 'Xiaomi Redmi 4A', '6.0.1', '1 (1.1)', '2c48cce57d24', 'cAexpFg7Oqo:APA91bH932hp5Oj8rDny1JL6Oa0zF6njry9jRdzKXZ_p8cIJpXZGjv1Iq740NQU-FvaCwxG3mW9jpKb1DjGMcnZyHd82h6iOkk23uzFtWqnmq6lCLN-Uco2eZojNgmD3NFSRMcVIodFA', 1554354145341, 1554354145341),
(30, 'Haier Andromax B26D2H', '6.0.1', '1 (1.1)', 'c772f2d2', 'cxZuKCS28L4:APA91bF3YLD1yOeX1b0D6aun3smYduTWFREkYqfIAsA63AAqtF2186GKaYMB6n6XXJhjgYUXYtfRex99Jhs-gM4m2A2MuLCfTTB6886GWw0aj5_616KD7QE3j6dQ3wbCy0ytoLF1_O_c', 1554446326988, 1554446326988),
(31, 'vivo 1804', '8.1.0', '1 (1.1)', '5474028', 'dFhRLtp7idU:APA91bFIsqJ2yA8bKnIlawY5d9XreQ1QNMqhRw0iFaqmxkd_Xq6NpEs4aeMYznVsTdTk-CbmRlZ4Ehvc2USVUItx-VfBugjfwVW3yYhZq6Ato5LkU_DPkdwwRxRCF9iVcbyRtCPYfeMB', 1554616053639, 1554616053639),
(32, 'LGE Nexus 5X', '6.0.1', '1 (1.1)', '1f4001584b3c229f', 'BLACKLISTED', 1554666912939, 1554666912939),
(33, 'samsung SM-J610F', '8.1.0', '1 (1.1)', '3df5e4b1', 'fI9QwBaAHMM:APA91bGJ8obpLly9G6Xn1AxrMrw2dSQ1oTxu3YLOoAiGf9vs9dL385y5KSROAF5hfZcIapWmVEjHd7u52DFfNd4cuzoeva58Kcw_hYTy9SA0Q-kwLmEAoqZ4JLq_-fnkKIk0tkoZxfWv', 1554710053666, 1554710053666),
(34, 'Xiaomi Redmi 6A', '8.1.0', '1 (1.1)', '9823077c7d23', 'cEV6vsDKvTM:APA91bGXpPtruq8bbNDFkK60WnwqnG0VDzhQXLiHUXjSqL_esiLqRBsIkysex0ot506-cNlgQ0z1_qKYq7QhwgAsDOqcoEdkNLhnuAEYxlXbV1jpeWmu2FslbTD26HRh1j0wyYuSDSoQ', 1554715627761, 1554715627761),
(35, 'Xiaomi Redmi 6A', '8.1.0', '1 (1.1)', '96d3961e7d23', 'e-KBnL7ye7U:APA91bErtfCVZ7eSQw5-lKCcocXBgoApt9dSIoNMacAfxDTnoL9bg2aJO3EBYegjK-VIAIIKZdZFC8m1PhYRDAS1pwzlIFDZ0RgCWRac584RmCQ1YJt0HNqlnxzkxZrXjaYitfDeI2UL', 1554867602027, 1554867602027),
(36, 'OPPO A37f', '5.1.1', '1 (1.1)', '7133dc9', 'eC50zY8Oj5c:APA91bFwcerweUcsxnFAPKk7pwC_RswSGPEloONpC8f7rc9GXevdliRhAu5Z3q1pX0BiGdiBU41Nds66ovHqSYKcldHvTbEGKvqbAvUnnxV1wZmM6J-zteyWvIwnAeBbpp409XpwJmoL', 1555052270856, 1557993103695),
(37, 'LGE Nexus 5X', '6.0.1', '2 (1.2)', '452c4fa19fffe346', 'BLACKLISTED', 1555065304377, 1555065304377),
(38, 'LGE Nexus 5X', '6.0.1', '2 (1.2)', 'd1c686b9a7896314', 'BLACKLISTED', 1555065400056, 1555065400056),
(39, 'LGE Nexus 5X', '6.0.1', '2 (1.2)', '7de55daf561003a2', 'BLACKLISTED', 1555328653971, 1555328653971),
(40, 'Xiaomi Redmi 4X', '7.1.2', '2 (1.2)', '4740aef37d14', 'd_jFQfp8zq0:APA91bEl60X_v_DssAX2vnSQAyzJhfM17GJMD5QQM8TsPjL65aqvuD3wQZ5HcGS5ZgBWEhrFlWvrw0XCOmbVXpV7BwVy9E_8Arg9SlSVys19xUC_DcTxdDuW7cv9PHuj3IVXLXcZW66Y', 1555465083372, 1555465083372),
(41, 'Xiaomi MI 8 Lite', '9', '1 (1.1)', 'ab8803d', 'd6IGbLCsh9I:APA91bG5ZkULOopUO2nyB4FUV3TpSVk7xOzrLHiOgtnBbEVW7U7GMFoCTJomWATpqWzEawAvHFIiwS8qVzy_9XiE7_ZqGuI-D-nhFypy3Cln6EgkaUJuWnQeTVpTICDNS2uwkk8i5VoG', 1555742857471, 1555904199891),
(42, 'samsung SM-J111F', '5.1.1', '2 (1.2)', '42007b1acae8c400', 'e9srIlDCxK0:APA91bFo4_uJl2NkaihAqKd6U8Qs8klD-fOqY0axzcxdw7Hj1SpNsJ-mTPr49zpQBI-1cfkAsfgguELcZXOE_6MOo_3NkNYQheW5F_iEdCDZ7rLTpEWrbIp_tuynt9RmIghBonDBmJO7', 1555829971266, 1555829971266),
(43, 'INFINIX MOBILITY LIMITED Infinix X622', '8.1.0', '2 (1.2)', 'c7c0227f', 'cmRLdV_fTzM:APA91bEWW-1z4W1cCYR7xAQLYkWbPy0jNO2U8B5GOq5YycccgtaGy6Ve6AGrjbUR3tf-JmsZcH13q2G5C7r_Y9-QWHlkV9em5H9FFtDv7dSUnhOzC7X1Rhr0TflkQSDsTl6qgj1SVYlg', 1555832572709, 1556867077419),
(44, 'vivo 1724', '8.1.0', '2 (1.2)', '3450685f', 'fzadJNi1YkI:APA91bELeZwgZJs_7P-1DhP6xd6ED6bLIty6nA0MDz-Hq9uOmtbFe1drAKJWDGt_secQk0mt_nHTenE-1e-byxIT0CmuTWDaa0B54rWNB7eNZswB8Xk-lWxC8rzo-U3Io7XPugAsXnh4', 1555867303311, 1555867303311),
(45, 'samsung SM-N950F', '9', '2 (1.2)', 'ce0617164baa2d17027e', 'fq05-vuBHTo:APA91bF53uAfh202Tzln_B3jL51BNFpv9EZNDoM74EsK47WQEPe73i63jTPi8dhhB7XDWPkgfYztJiGykLYCEQwTvVecqPTMmGNimb2mSzBH0T7quHkXh-Ta4udKOUKVCRfty4WJ8qyD', 1555927679232, 1555927679232),
(46, 'samsung SM-J700F', '6.0.1', '2 (1.2)', '3300a3e8a897a223', 'fn3QI-sfQ10:APA91bHQ2zTX8H5ehV8HWn15dgKfkIMgQEiKtLkiRgcii3GHFt5caVAFq-jRlwonGPfO1u0xx6EkWbNbFnFRUO6tJ3ybNnXTdm_ejnlnz_LgK0bHOI57gmk_Ot6wrvtfGQKDuRFps7o2', 1555929154257, 1555929154257),
(47, 'samsung SM-G532G', '6.0.1', '3 (1.3)', '42000327d42a25c5', 'cKTxBCrG_Ss:APA91bEWm0BD6hTOASNAcpt8oqAoR-PHa8p-A3z9ljJ46QbyvpQfVJZVQ6pF7lZfYOWmwOphLPgyMj_J21ucUbNBrLsMycMf0QnHPIoG3ygN9hvgp9aZGuiN1z9BYUbfvzT0hlW9ssvw', 1556690263571, 1557313703583),
(48, 'OPPO CPH1723', '7.1.1', '1 (1.1)', 'PJFUPZC6BUV465KJ', 'fxK5nVAQMRc:APA91bG05dTuAzNZ8KDzPDWxg-iGBaEQUXLz7_wMlmeTdsLnjbIOxw7zstL_p8wjonXhFTFrkwaDVphWwhrPd4LOaSMQSUOXvRv8AXbqp3MoUNl8GJYvhrTSOzIR_5NBLF4qVVzrMYfv', 1556694951900, 1557576568227),
(49, 'Xiaomi Redmi 4A', '7.1.2', '2 (1.2)', 'a818a80d7d24', 'dAHjkdAbPM8:APA91bEA1S-z6LAvILHWjXCG0diA8nrWhKyXGu6fFk1miIeA-IyilqnXKC719x0PXCUdyJCqH887FvhKO9UxbCA_PYkmuzqHb6fHdxr-4FbihqldQtiWCNBXwQKEDNUmbEB0oBhMqiwy', 1556697150728, 1556697150728),
(50, 'vivo 1612', '7.0', '2 (1.2)', 'S4JJZLJNR485YTFE', 'exxZEWp6i_4:APA91bEq5pqKkkmSKGIR6d49p_0zgkSNogbeHZrX7KKFYLiMJLx-7Kz84W7Zed3JxUa0g6Lfs8tfOrLBMtXSJ99pvNmKTjd3AGctCcLqhd2BF605IErAiVSO4Le7HAq04thlPeMbBn17', 1556781356886, 1556781356886),
(51, 'LGE Nexus 5X', '6.0.1', '3 (1.3)', 'cd413b05ae36e691', 'BLACKLISTED', 1556870433405, 1556870433405),
(52, 'LGE Nexus 5X', '6.0.1', '3 (1.3)', 'd52b3e74bd3edcf3', 'BLACKLISTED', 1556870448137, 1556870448137),
(53, 'OPPO CPH1823', '8.1.0', '3 (1.3)', 'ZDS84SNV5DKVDYQW', 'd-8PzDpG4CQ:APA91bFp6WV1gpM3TP-lbiZelLsXN_1CQvS8sRMpWBXtPX7OoQrf2S5sQj0bI3WIc-ROm9LO6AgLbgdMZ3RcZgtLpmUofKsq_cBx0i2cGMHHM4dCK-Xnm6SGpH_SREEOW2Ju78db_hHe', 1556891794030, 1556936226707),
(54, 'Xiaomi Redmi Note 4', '6.0.1', '3 (1.3)', '4751da980104', 'cupKLLlgEIs:APA91bE9cVLmwtloPrLOBzFBVeSxgvSfTVtwFT0YJ9_U98H3WCEigwcdl4Va5dm9YnOmpcPnlGKm8hHb5dSvcv0vbn1m7uU3Kk3qK9YhI9oldOwwjbmmEgrIJInzu0xz7DUSQySV6-NT', 1556932322543, 1556932322543),
(55, 'Xiaomi Redmi 4A', '7.1.2', '3 (1.3)', '815757e7d740', 'd_8A8sPIALk:APA91bHp45YHPSaAM1TjQVAGLN6SmY1BWozpmNvb53pZP6OoUigIAEsyQny9ifGKv287xXT2TRI6isqPS0W4Cr4eiZ4V2k-yPcCMGUXQNtx1rn30o6TZriczYhZFyuDLblKRUi5-HVTT', 1556959013832, 1556959013832),
(56, 'LGE Nexus 5X', '6.0.1', '3 (1.3)', 'adde5d41b8749943', 'BLACKLISTED', 1557104812225, 1557104812225),
(57, 'Xiaomi Redmi 5A', '7.1.2', '3 (1.3)', 'e613da137d94', 'cgmajYBAiOY:APA91bG0Xl02OwmffxQvTgbrsjysUC5fMmnlcGRREBtyLeici-m7a-S6zlrlHf9pkABH7lUxAkekjK4BpoeiqlWFrxRXMqpyhjCPyKXO5psE1BZeVcbseWOJfYvjCGoZiEdSEqG3FDHj', 1557109514096, 1557110048567),
(58, 'Xiaomi Redmi 4A', '7.1.2', '3 (1.3)', '79867127d640', 'fluMv_cOcCI:APA91bGMVJXUCwkCaFSJlQjau02yNe3MxHU5bloMWJhvSLC7K7CY_WT9DKoUaumH94q1whYx3i_xVpx_gj8exw-RFG5H1-RMVK4_J6hq4F1Qola4eRdfrnnIhTNdW4O8K69AItQDAvCr', 1557109984454, 1557109984454),
(59, 'Xiaomi Redmi Note 5', '8.1.0', '3 (1.3)', 'bc6a0c5', 'e6CwPGD-slg:APA91bGHh2D-dBwRyoisllL3fxWqcKnlcaFZ5KHaa0uELDpjaB-2fau5rMbN9OHCxAznLWcct8C7_HbsEWXDRQsyq3LIKn6Bv8BaSgPD83hULdygTIz2_TiDFy8BH0FUwp4e9MBzW8jx', 1557233833942, 1557233924457),
(60, 'Xiaomi Redmi 4X', '7.1.2', '3 (1.3)', '442503d7ce40', 'fTXh16oxoe8:APA91bGXGPnWtt3uUXI2CvOQgwMW337yQLOu2TKjXfrqBBDlb8B5wFeVxk6hASxFcOWjtCm-OqSCrhwQcQ9vP_t5vpUok0SdBzS5Om3JNcVPoSNUW-guLo-xKXGtIJ6RJ7-qFpmAHF_M', 1557544437682, 1557544437682),
(61, 'OPPO A37f', '5.1.1', '3 (1.3)', 'cc92e6a4', 'dH3sKH1YRN0:APA91bG9J-f3wjsHbssBsOD4vhR0SdaCIdblvwI7FU_cTetaC65aazUIVjDToKWjFwmH8c0J6P2TO-NnltAd8sl31HuV_i9ee5LNksLT1qXZpng86xJuyo0E5Q0QstudIndt4Vgq8Adt', 1557566819654, 1557566819654),
(62, 'samsung SM-N960F', '8.1.0', '1 (1.1)', '25e43b44420d7ece', 'cOulU8_cdZk:APA91bHJSRpdiVXyBqdSMxMOx1hO-DXZAEjCbt_IPxCFzakuzOQ5kaCR95-zxcDfNHFMbA8cRaC_q5_fd_4IelTdw8w_04tV7palQMbk0NbstSVzPNT7Kd4HZSwHYWGjx7OK8PoK5olD', 1557577247190, 1566890231038),
(63, 'OPPO CPH1727', '7.1.1', '1 (1.1)', 'BUBEHMO7TOGMT8BU', 'eEq49kv8SNI:APA91bHexLHLAtp9c-NBr8OPnvzSvFIDCQqIRo2J_zvLgE6VhhuhxssOM_rAp0rTZQEXEH--ushWzxL4xzUSa4Z4ceREkirBjbTYop48hPsHm4kW7ulnCbgn4fjOwU6c9gTDffZy3f1G', 1557621121275, 1562681744492),
(64, 'Xiaomi Redmi 5A', '7.1.2', '3 (1.3)', '9559b187ce50', 'cQms4XQxWw0:APA91bEgnLjriMwVlXvbYtOAb9fn--tKhN0DaqWdrvBVr9GMhNNAslsGVGtjLs0GKYW8AQSM2LpYfURwZPSTxzv9H_nvcmNPBLV0hFUuSkMSil9-VchV_CdSxWweWX0amZJ4Qk8qxZAy', 1557649843361, 1557649843361),
(65, 'samsung SM-N935F', '9', '3 (1.3)', 'ce05171545afab0d05', 'eIXFF1sVpgI:APA91bEZJhGHeVvv2aDhK2vN5WJeOn2v52FUALYA5DLJFhD5N1FwPXHJhMnShSDUup8A_ad7Fe0bEyhHnFHkwHA80fpCG7gY80mqEBnRXYjkbyxGvjtRBYULQutw7iIhRFY0T2ZeyaY2', 1557667500542, 1557667500542),
(66, 'vivo 1902', '9', '3 (1.3)', '8L5DBMNZTSEAJJTK', 'eZXcZHcbzIU:APA91bFGfCsn7CP0L9avB0xR9V4O5VbSNsiOfyX2ODcYXRbCTK5VY4fM-SJRyLZaz4i7SMgjaEVE6lp6HLLLZ8SwpW7eAsH0_G93MQyFrzif22gmTNVWuxKr8Ns58WzsXyciYM-HGs1H', 1557845115868, 1557845115868),
(67, 'Xiaomi Redmi 6A', '8.1.0', '3 (1.3)', 'aa6a9bd17d26', 'c3W9tJ2dJHg:APA91bECRKtFiQPhgc2OMaUbB1w4JmHi4QEhXGAV8iLPRiYRvHu5CEEh2YfbV07K5i5jTJJdwPxmEwmu6vyxUA2ZK5uEsfUzVYTw53wIkF13Z1jdqSsBs2hJ-V1265y1EehaNzk9g9wX', 1558008096159, 1558008096159),
(68, 'Infinix HOT 4 Pro', '6.0', '3 (1.3)', '025761181I004327', 'd26S1R_9VWU:APA91bELgiHL8c_oteVHjxE-JcqZ6-AuGvKKkDhhPkLrCAcAFs3JFbtVoN6GiYl4SkD1VanYKPDE8SD46-eNZ0tSPoy1slZCfg1tb3gFXzFbBW6kLqdExXkQCw4tho0q7-fE6Y2U0lLF', 1558020017800, 1558020017800),
(69, 'samsung SM-A750GN', '8.0.0', '3 (1.3)', '32008c6ec015a515', 'di472NNjoZg:APA91bFhNqINLXNF7Zu-p2d44_dvobl8bzI_iksPqPhK6g2MzGIYee0qGZwN6jqdHLis4cyKlER9T7Md5K6cnKZh-uQxAQ1gT9MAZHcd2pN4hebs_Rr3JPqe9ftwXEAcJKK_QCxvaeKj', 1558464580481, 1558464580481),
(70, 'Xiaomi Redmi 5 Plus', '8.1.0', '3 (1.3)', '35d895750904', 'cqCEtHNeWrg:APA91bH1fNaV9B4u-b2TDWLtH9hUIXr6tgXujQQj0yevWwHxZ9bjp3-UQPT5iQm3KV6dEM1kd4Afy-wOswrbkE7FQG7FdzOEgrtrbDDCBcl0x5QRnPq3T4pYH-CQrpZPAd44XdNk74j0', 1558561942236, 1558561942236),
(71, 'samsung SM-N950F', '9', '3 (1.3)', '988a9741584657583430', 'djBsZIsdMCE:APA91bG80L4OQ5lOiWVo7_eCKXf0AscUirLpy1MQNg9vyKbj_esgQLLVf_bo5sfKWX0-6Reve50PqZ5GBrZH1QnMax-1rsI6lA-O22D7_w0be0M-kdziMAyqw3BC7i3TLeRWTgUtrJl9', 1558852810651, 1558852810651),
(72, 'Xiaomi 2014817', '4.4.4', '3 (1.3)', '3e38fcd6', 'dKYhgA63R_A:APA91bEQxPtOnCJeDUNwtyx1EPJnyREJlQwh2VsHoouP54NFI_g5fkafV4qZWOszRQxwRFcVcJ_B5TXezJN7fJ37BZIQ1gAJl1QEsh3QJMlG6zHKE-i89Hw7Gs4MmCXn5TXrRU_xQiP_', 1559424487662, 1559424487662),
(73, 'Xiaomi Redmi Note 4', '7.0', '3 (1.3)', 'efa268b59804', 'co2VVCI2zSE:APA91bFhaIyj9PjwzH398bvWQu-sk_929FoME7XCMdN794NaGvcuyCeuApibtmb-pIVrY6zsZfvNleUtu0J2gr10uEojOBALCf4evEYHVFHPyqe5dWjfopp09VelEwojoSgbjkrYcXRV', 1559443335565, 1562230177835),
(74, 'asus Zenfone Max Pro M2', '9', '3 (1.3)', 'K1AXGF04H298J9G', 'eFBitVS62QY:APA91bHdJShlTfAvlcFdAFh1j24DP97JN77EhmGuTPxNuMh3GDYPtYydJnGFHpXuR3ZTg8S5iS4tQsK5YQlGRK2dni_Ya0AA3_smdBi-hHpXTpJIrgFVOKVuhsk67kb_6ai96b9Af6AL', 1559992315950, 1559992315950),
(75, 'OPPO RMX1807', '8.1.0', '3 (1.3)', '38e92979', 'eVRRs5rcDFk:APA91bH0Y0DmZPXmO2-SwdYiJ8EMUVkvAH-Atm-2G52BWs8_4aWX6qh7fKJoWwDkYc7nK5Ot4BgkiejYY72uXXIbuD3k9vV5sSgCE_dQR-bJH6lHaCfqvqAlutQXaRCFHx9n_OMxlC4L', 1560244403719, 1560244403719),
(76, 'samsung SM-P585Y', '8.1.0', '3 (1.3)', '5203a29bc0e9241f', 'cR081f42V_I:APA91bHnJhWyUl2GGotQmiP-wOuykYyk1ZzuG5Rqe0ZSxjN1Kn3p_Lzj9oapubjh4XhZHqPk1bWTRkjNSYs4_2Txr_4RcYwoD1GLyd75sK2EPg5sIXUHxABTv0KPaSocOdWz-D5fUxU2', 1560397588657, 1560397588657),
(77, 'LGE Nexus 5X', '6.0.1', '3 (1.3)', '63d488a285157923', 'BLACKLISTED', 1560636041081, 1560636041081),
(78, 'vivo 1606', '6.0.1', '3 (1.3)', '219150fe', 'fnlVxiT4gJg:APA91bFjW8h4w4ytwr58an9-Yzh9C3d2X4a-S-tXMOfjbx7sfgnp4ba8hQ4jEJncMq7ImCQaU-p76jp75SVRKePjN3zvSEiKUB_-BJQnGPeo1gz7QPMLQO342kox7pTrTBk6PKzZDhRW', 1560661798589, 1560661798589),
(79, 'OPPO CPH1723', '7.1.1', '3 (1.3)', '99JZA6J7EMAALJKJ', 'ffhh47uMiEk:APA91bG0_22VBBitRSQGSDc1TA_g2Qjlm0ir9j4SlgTLnUiziJfh9IYviyRmeoK7iJXy_RX438RwpXX7F3UMLhQiZBGN2Ldu-BuETKi5h-p1OTMMmCQemH7boLLnCTjvUfK3Eokn9f_t', 1560755604916, 1560755604916),
(80, 'HMD Global TA-1021', '9', '3 (1.3)', 'PLEGAR1771808314', 'eHoNeOWOPno:APA91bFos0Sz2PfiM5Pwck97NLjieJ8tvpHK-qAaPGfTg_hUa2UIlxmO5H-cAda4T09zfz7g4O4NvoKHW7tTGscbQd8uplDSNviGvSWkm9JD4F7X7v1kWkg6XX_KuVELZbug80f7r4eP', 1560770428176, 1560770428176),
(81, 'Xiaomi Redmi Note 4', '6.0', '3 (1.3)', 'SOWGRWAMA69TAISK', 'e4AQ4Q2JDx0:APA91bHoQGz8k49kJQY4_0L0dOALXkVpsvMVZ4wPHytgg6L0f-qiuZp7lG2Cdd1GWIZMUPswB-xzAVFo9bsVMbuXmwcxfGhgQJ5dg44RpH9JD5dfzpHJfTPka7gH7YwcrPmzKgEDOkaj', 1560781983965, 1560781983965),
(82, 'vivo 1606', '6.0.1', '3 (1.3)', '21509425', 'e1OeMMdVcMA:APA91bERa5m4t5NekS8CZM35wxXUCiUqXYoK4MY3BLsoH6z_R3zHNU8cCQ1izPJ6m_AeZU0rM9d9xh9xKvDpl_hdvPrTmxANUJRdKuTMCmC3TZjGmVErAw9y-emXOZJgfY2zHbITSv8d', 1560868180136, 1560868180136),
(83, 'asus ASUS_X00HD', '7.1.1', '3 (1.3)', 'HAAXGF00Z5188TN', 'fq7S42EOJh4:APA91bG8pnBMDkZwRyLuN1Mh4EtTIVRgNM0drYxVu4NGiw2IJlGMRHEn-HI1ofxfTmKAVZnLIpLq--TlJG6sfk5WluxwC52jZdbFw5s41bw_7nzPubEx45NWssRrQr6wdEVUkL6nGBuq', 1560886086117, 1560886086117),
(84, 'Xiaomi Redmi Note 7', '9', '3 (1.3)', 'ba6500c', 'eF1Ke21EOZw:APA91bEwrhm34V3oToGO9nXeehr7Qil1a-mfK_k-CC3xX9lTpChNUabv5NR1SGUOYxriiqPPjWkAWb88o19b0VjuW3Nd1tADeazwr4QZ51Z1zAVs8nxhMBO2Z5GZyX0Coe4Da-zaAvzl', 1561116640831, 1561116640831),
(85, 'samsung SM-A505F', '9', '3 (1.3)', 'RR8M30AXR2V', 'ernMN6eBODo:APA91bGY5Z-jfnVMgr0-1YL_gbj3hpo-CNv6UXvAyhgoplssJ1E5kTEzCyU_M4d1464xSDAKaY6TTtVPcTd-TXe3SDRUuTHPstw1Lsz4GXacuZkr0Cz_LrnnU9B0bfK2EtU0G1_UY5gM', 1561188960714, 1561188960714),
(86, 'OPPO CPH1723', '7.1.1', '3 (1.3)', '9S4DGUBUJRDU8DYD', 'f0JDErz_jeY:APA91bEEdRrz57gl8PLrJznxyKuZ7bkAnWygrNpECkwEioUm9U3TZdOrb5ZwgPJTEBdGBGkF1J0HklWY3MsoMjisSKNjJ9s7I-EYFgGP4tBJxzh8ywmZr6xcn24OIqrvQsHxu5HCSJu5', 1561352759895, 1561352759895),
(87, 'OPPO CPH1909', '8.1.0', '3 (1.3)', '8DOBQ4GEZ5YDQKAA', 'd2sjeqG6e0A:APA91bH3_6kSuG-vdhl3k1Ye7wFVinwIdU0ak_KDx1h18hOBFHvrAwstQ7-uRtjv4DoHzJfQvYOPV9j8Wvg8bekWsxw4ObJhdM6aZ8_nL_yJewr50Su9QTuKLTREpHihbpvLItd9WEte', 1561369283618, 1561369283618),
(88, 'OPPO 1201', '5.1', '3 (1.3)', 'IFTGM7E699999999', 'dUwq1asj7C4:APA91bEm1-h_WmTzc68znNsV8LoV0VdVpy3s-e8EsnmIGvltzyGCur2vESZMbkMMuQC6r_VK8lCfmXxTH9ryc03wwlDkpnC45qqgH9JP3FWRIQY4bLoToiJScB4YVYaztyjh4oxfs0GL', 1561424062168, 1561424062168),
(89, 'samsung SM-A105G', '9', '3 (1.3)', 'RR8M30J8H0E', 'dJZt99-mDYc:APA91bEPaPC_0fjbjl3PXVcizimc8KAMq01HmxppL4FVksqwk7hmAatiyigKFatp7ox_AF2AakIGMnl9Sb3btKVO0gipJWp2aP3QRjlEg7dopevKAYSC2fqFCbbq2zVLjUDmijdi4Ov9', 1561517152410, 1567750168212),
(90, 'OPPO CPH1911', '9', '3 (1.3)', 'BARO9HJV49I7SS59', 'cn1aEdBuuIY:APA91bE1YThjV1zJY3KtACEC4TN323jVnwFw8tXett8YchOrFm8crTeEkstT5648crxV0QJ45Cg9W8kwafMDmWyZMI973gq14C7NNgbcSy8_DxzeznHSjZH1QJc3oPOLD_GmBAM_Efhq', 1561528196678, 1561528196678),
(91, 'OPPO CPH1909', '8.1.0', '3 (1.3)', 'PBJFTOSCUGVC4PTK', 'dhrFSGjd6uA:APA91bEuSYt08P0paAIh_Y9Q4ylLSCD7PmJIawWsFg6X2pu2xmhu07iUVhQo0QUw_-aMEioT54_WgqdH-UJW-jSQA_h5RnKf44vnNIBgBaKTgbS86mxtbLqBL4X7KLPhep5L7yRR4fuC', 1561625056217, 1561625056217),
(92, 'Xiaomi 2014811', '4.4.4', '3 (1.3)', '2a20860', 'dXTUnzM6emw:APA91bECmt5EmsWmAFrWgiE-uL7QRSLBe8weZHp3n8tZH7Wj3Ao5tki8gOQ4VghIApCGVXInekHuv9xZGZpWs-oeimhQqJesmZIT6I-nrgJWRfac2MvGlNSaLQVJwxpX5uy0ynOIgPxO', 1561660009591, 1561660009591),
(93, 'vivo 1814', '8.1.0', '3 (1.3)', 'HIKR7TKJON4LDIR4', 'cjXWA81k40o:APA91bGBMMrt4PPHJnebeXtthxzUQ3j8g18JOQdLiKd0ljtJZn8bc_oIMWw3qCoHPvkxf9ReRtuBpT83mBRVhcJbsuVnAlwPoEnffhy5VSf7ccmIHAmiaU2O_eYthVPC2fzymlJreI4m', 1561694942925, 1561694942925),
(94, 'vivo 1612', '7.0', '3 (1.3)', 'CUNNAMAU99999999', 'fW0Azq6sows:APA91bHsw_d9cOuSxwTvja-lwtGZ86sNlPc4gqxyxXuYZVBRbi4voKJzrqKC6yW_7GcoU5RAtHstE3XYNBq46Aca-eQukISoiVOtibolU-igGWNMozFXoI01mtj1Ez0O82NfvgfXJeG8', 1561715246411, 1561715246411),
(95, 'samsung SM-A920F', '9', '3 (1.3)', '2b45f7a5401d7ece', 'erhyG1zl-S0:APA91bH98wx_mAXvJBAV9VbT-ld75O5KXC-Wco4l2pbpaigQxcGzCleAJXiKdG6gvJ52hODJRFNi-nsgUXmXqlIUaGRI4i_k5AfwQxJo2DEUVuXqEgs2lU4j65cyCIufFN76cwFYDX6n', 1561889998162, 1561889998162),
(96, 'vivo 1727', '8.1.0', '3 (1.3)', '8e76cee6', 'e4D5v_BjGT8:APA91bFhodWwaEn2MvmYDWGVfkdm-doFDSWfQrq-994jErJgQLcctn9V94gcLC5zO60RAjVgUNVsClZKEUPobT-x5Yfc01rrMwGf1xCjJE6i11yss5P6dGu2bUsMfmroQS7S3HrEcipr', 1561901205687, 1561901205687),
(97, 'samsung SM-G532G', '6.0.1', '3 (1.3)', '42006533de2c3537', 'f4irQKDEVxo:APA91bE2p5mtMTEPXPacSScA04Jwohn86Ew0yqN9taAk2DjTdFg_eQxbz36e-IOhaO-InGzHcg-6mDxG3VkcW6ugaW9Y1c9bQrNOkl0Pytr-NyvpPGrdOH5rOZsmJJ0Exv-AkmC63ztF', 1561965838364, 1561965838364),
(98, 'samsung SM-J400F', '9', '3 (1.3)', '42002c20c89a55e3', 'ct-wPr6QDVI:APA91bEnLgDLb6PeHaJlGxMYcfxaqauak-VgYgB8wCSYSvEDiqtF1-FAlVO0MXzd8r6bwmZhGAS3r_ylkrmWDqDxyo7MKBH1XxKoC8_p6qYpXluh1r08FIfIPDtxULL9oxhawDNEESj4', 1561968493570, 1561968493570),
(99, 'LGE Nexus 5X', '6.0.1', '3 (1.3)', '3bb3c6b838830c82', 'BLACKLISTED', 1562176540188, 1562176540188),
(100, 'Xiaomi Redmi 5', '7.1.2', '3 (1.3)', '6ce5d06a7cf5', 'c6jIxIzuWaE:APA91bGTuIUVgfcVUT3Ai6k0vmzkcSpShTVJqo_eL2tc4rbUgmtEkdJtT8GW2ypf8rc3-NIGtpQUUvUG1O9xSYxtWjutCukfwWym8UJPgbG0bUGfSX7mMwiDKv0fy-SCqfYUfuTkvpNw', 1562210617349, 1562210617349),
(101, 'LGE Nexus 5X', '6.0.1', '3 (1.3)', '10d9a26d011ac618', 'BLACKLISTED', 1562212454607, 1562212454607),
(102, 'vivo 1804', '9', '3 (1.3)', '757d44e', 'clKRFHzN_sU:APA91bHEG_p8GfRac3NMx52-x-8oV2TZKd0AKQSB0UJ9CZoxjmmvydeuRpnCgyTXX_jl3T8OxnTUfyEwlFvwWw-QzzswQDjmYPMqeGF9hphmwPpmpnzQhyPH46gMH95_RpaBkj5M95cc', 1562467248412, 1562467248412),
(103, 'OPPO CPH1823', '8.1.0', '3 (1.3)', '5HLVJF6DKFPFSKLZ', 'dQV5BO1wU78:APA91bE3hXbfYCZP7f6e_ym9hPqoVTAd-OzB5aER2KX-2Qyp2feXXvQ1Bt7Ggysg3YBZnbv2JqFH5AYq8h-Ev-en1aleW9Akk7alVcvaYsBjd_3vJ8bhJuDrban08wez13pFuQgRTwdb', 1562474710185, 1562474710185),
(104, 'Lenovo K520', '8.0.0', '3 (1.3)', 'bef4e0ec', 'fHWudnyxZVA:APA91bFb_VwPju65X1GvsvyC7RGX6q9dRqALxr0asNbRc06kZ0d29YY7XJtDPVfb8F_o9DsSwTldKdpSXRA_jaWZ6RTThOdbfbWkiOYD6iQNLs-ZvvOrcBb9zJO5SrHOvk2W6Nouekeg', 1562494773954, 1562494773954),
(105, 'Xiaomi Redmi Note 7', '9', '3 (1.3)', '3fe4cc1c', 'cjUtrBHlufk:APA91bGa9jnTgUvgD06aaac0AUqVItyOBHpKE0sB3AqDxdZwWRtLakuGkHtK5RwmUZ87a0km228IKQHh3Xf9n_nlvew-euXIt6sCGNdgaXJTFmXFPr2g8N-ErB3L3wX603BsBoFPKKNm', 1562512526729, 1562512526729),
(106, 'vivo 1718', '8.1.0', '3 (1.3)', '35c5d86f', 'eY__UlDE60E:APA91bHqccCj24R4U8vD0BFr5O-ile0_rV6VpWhV6nT1D2u2So5Iz7bA-JguT-CWD-3uTTXZhmC-un8S_7Ssuz8ETXFseakv-pzG47FmyI1dGntrdeNqpyOr72yUJnpx7VFjIkc1q9VX', 1562544158263, 1562544158263),
(107, 'Xiaomi Redmi 4A', '7.1.2', '3 (1.3)', '63fef05b7ce4', 'd_GVCQLiWcU:APA91bHVJZW7BwRgNocyFI4rigBJTuQNZYaWW7YMqls8r-BdmIkwnYsLkOVn8E_HUemeK5qMrBK-H6O0j0nXSBSSWTZbmWyncX45adqwzRlds6sZgUP4A-7MrmPybyiXK6UjGKAZQ37D', 1562554854865, 1562554854865),
(108, 'OPPO CPH1909', '8.1.0', '3 (1.3)', 'FQGESGV48DGAZLAM', 'dbFq1tjLX2Y:APA91bFJDy-TMpvkvdPaU-qPPi_iVtfMzIIgspWEDH8jne8r-dH34v59hpfajCzTHnR0p0XpKGNA0Nh2zFZa6D6YjbmpmfjrFCIGP-PwHId_C_DpkgxAgeas5GehhMQ_uqBhotpwWJHg', 1562565607126, 1562565607126),
(109, 'vivo 1612', '6.0', '3 (1.3)', '4DQW6HCA755P7LQC', 'fHVBVFd7zyc:APA91bGlAPO4rJGFDqFoKKDcsR7ECmowhzj376H51CNGEuVamnvD1VjKMxURKBIAbwP5gvbB-hAVipynRY429xFLLdxMq1tukEzC_cAOY3KhVGHwlwrvCYVKOyeA6DNPI0eb2OO2HZOv', 1562600343310, 1562600343310),
(110, 'Xiaomi Redmi 3S', '6.0.1', '3 (1.3)', '1095ccee7d63', 'fD9di1jG67I:APA91bFPWn0XmIkCTIP8k-BHVZw-VCwhJaKeJr9iN0W8i_szGHeV06W8OeEsbxou8NyjgFJrC8kfFsZ32tFSacc_TjJTV46kaFBnzwUuRLov9vEijFGD7cQDJ11S8zItCXSy-oW6qsf-', 1562807438872, 1562807438872),
(111, 'Hisense Andromax I56D2G', '5.1.1', '3 (1.3)', '8563cad5', 'efwd2z1kFgA:APA91bHjCxApaxUPqCsU-eppf2JFxR1ruP-nu8EBVqViydYHhLa6XcpJEXjksoZcAqZDljQLw9rU73tliPRVYuUs91Y2_6yXRZEKAJ0v3o0YZW245rjqAtErxE9dzpPpU1l1bcercgxL', 1562867315109, 1562867315109),
(112, 'samsung SM-J610F', '9', '3 (1.3)', '7962d874', 'ccDqzCAuCVs:APA91bHqVlPZ3pCZM7Q0QGurKztSSDzsltDD9vQRTWbtcg9koCksYkyaRwhZwAjjXN5ZcJnZhssVqY0ribJuxZqRrFte9bi1KwKeFhnn5kDGpUZEDqQozqJC8aCcZj_a0nW_vhG5I5mi', 1563067849785, 1563067849785),
(113, 'Xiaomi Redmi 6', '8.1.0', '3 (1.3)', '9438a8707d27', 'fqyaSFBJWu8:APA91bHF50dvPTZKk6usjeRQFir9cnUtUQCxmYTEp6-QXThzFtWon_VXcjCnPM7QR-RKyHfRcwJ_uB8KT5bI8ETCvSoowk4hTKVYe5yy4KqCi2EEab3pFWjQoNnuLqClth1I8LYuet9W', 1563275371961, 1563275371961),
(114, 'LAVA iris65', '8.1.0', '3 (1.3)', 'LCA880I440AI046A', 'dcHmJ1A--84:APA91bGmNKkbKZmtmCTSAVkBHUFMVMWKnK0dlakqK63_D3RR4hoD4GvRrPHlE8C8z3x9nd8gO9jV2us0lxvByMKNtr4dpQ_PziQnBZ6v30sz19zCTTlAxGgP-eKZJVKnpOQzTp0kISs0', 1563287866794, 1563287866794),
(115, 'OPPO CPH1723', '7.1.1', '3 (1.3)', 'U4QKQCEAPBOBJBGA', 'fbErVbtfIR4:APA91bFlcKi6WSWrZGI7cMZ5TEr9kx7of6ie_Yefbu3Tmb1egRZ7DnAmgDurKwnwF-vNvTgORsI1u9PCmLsU8gounIfnnek7V1UF2afYsiCxZFB85KJiRPGVvgfwdPGdei58zrlkdDFI', 1563288046547, 1563288046547),
(116, 'samsung SM-A510F', '7.0', '3 (1.3)', '3300b0a73fd7c245', 'ehJ-0UfW5_4:APA91bGCeIq3Ud_sjUcoJJ29wjAdnXxVywlVTuOiaswLuv_6xp2fQs9lpnBcqfdU5VJ3cw6cUYhSTxmj7HreZTkGAb4oIOvjV3vYQyy-E7NScb5c4ozkk09SIZSX9Asp8h9Aqok_Uy4C', 1563361246417, 1563361246417),
(117, 'Xiaomi Redmi 4X', '7.1.2', '3 (1.3)', '98edbe927d14', 'ftyHnEg98xs:APA91bHegMOVw2BR5yTw7iXW49NETINTOuLB4cK4QnfB3Zt6J0n5V9e4le-c4eM4VTXNRVtrrL7cQESyPfwMta8VN_djTb7usMbPAB39DhxgDhxfYDQ6k0de5EpJLh3X_VzDDcXQh4-N', 1563418171550, 1563418171550),
(118, 'EVERCOSS AT7E', '4.4.2', '3 (1.3)', 'GEQSAQR4YTWWSWI7', 'ffK7Ti-l38I:APA91bF6MaYI1DMarXHnKKW7rNq0o9IS20JmGBhKnACPXFCWW0WcSqSsL5DF3GSnN7lszNFKfVcWKl5UWe01bVqOiH9mJ07oV7Z7CBv_R92-ImNlLCoC1G-uCi0q_JmvzrVBGPbGabmU', 1563440431465, 1563440431465),
(119, 'vivo 1601', '6.0', '3 (1.3)', 'FI9DGEKNY54599UG', 'fL9_h008S9I:APA91bFXuWtCjieVxIPJFFRahVsjS0XYghYR59eJvHoOCQ-uWlNcJKcnISncXa3rTGphSGqCeqmvc1mf4gb2l0jLbL-9oO9RU3N40altWx2cpKSGb0fNeDgsBPXlI8KRVub5k3AhAHQs', 1563455413224, 1563455413224),
(120, 'vivo 1817', '8.1.0', '3 (1.3)', 'c157d941', 'e6Nb1YxBvFM:APA91bF2cvqXkygKtJ6YaQfDcQvpzvfFjTte_4xqbdJDPj6cWSki89LdFD4IC7ff3xLNmfLDVpGxKHI4CRSOsx2VFMMoEeyAwo9uK-jVIAWMt4kptMe9vfzuw2OYKlF9z8f587p4hbbF', 1563547327021, 1563547327021),
(121, 'samsung SM-C900F', '8.0.0', '3 (1.3)', '1f00fa56', 'fmccL7_F2Zw:APA91bEeTa9WXAhRnlWJI7AQpWcCKfPUSGaX_uRXf078V46xYWErltD-EPFNuPDtlCdbsI9bU3Iblugl55PJWtaeRHDdlf1kBnOggYHXOKalrLytVEPtYABOQNIwaSCp5NBr3znfYsuu', 1563596478291, 1563596478291),
(122, 'Xiaomi Redmi 4X', '7.1.2', '3 (1.3)', '289be1ad7d04', 'dOXuyZ8ssFA:APA91bEBHXwc5MV6v36dddFD7vAJ97QYzT27HUfltUbyfelZ7-Q8OftfFg8YAcyQt2dsfvqoBUvGZs-epofUzjKnqnfdwoxABY6BJKtj0TikoWv5LK_In2OwnJ7hx75pkowRf9d_peyJ', 1563708094054, 1563708094054),
(123, 'samsung SM-A105G', '9', '3 (1.3)', 'RR8M40KAJAR', 'er5URGVLxGk:APA91bFJXUmi-iUWc0fwZqm3I7-dLwxD-8Wcs_jZMg3XhdZWX3VG9urB6i1X3gE7xWMJovfZOlOsn2i9ydCY_o0NnRiiMZxXJCrwCY82qQMqEze97Cfmp7KPDBMxlybMyPjgzxzTFgks', 1563718513023, 1563718513023),
(124, 'vivo 1612', '6.0', '3 (1.3)', 'TGPJPBONNF5LLFTS', 'fNWZU8kDHJM:APA91bFhXks83sB4NZ9pntR1OOt-k9-ijgICVoDxoJDDKg-a8QZNTj1NEzdwb24MdmTllGLKJxlGNxyaDo-m2qHIU43uW8C2QfwMQ2A6jk7p_W98wHh8pDXabvTVlgIBuVtvQcS-2lXl', 1563752679587, 1563752679587),
(125, 'OPPO A37f', '5.1.1', '3 (1.3)', '4e41bd3', 'fE9xEJDfEu0:APA91bFlqMHhAMaoTyJZ38skRUSulskZUaws6Cqvq_PDyMZ6l3HKIcT7g-tJf5dCPFwXGWR2VknYtn4nHDaui9lx28y85O-pb1QhF7MoURLAQFA7ODsCE7onyI6O9DTqUvAlNg4lLqyH', 1563902438261, 1563902438261),
(126, 'OPPO CPH1819', '8.1.0', '3 (1.3)', 'V8FMF6GMCUB6ORU4', 'eGHHX0avcho:APA91bE_gNxE3yX537RBKNl1FyvO1cZnjbPh3rOejfZsh_7jkjLB5vqzwqwWWXfq71Yn5kjq8NAM0tCdLQH7Q8rQSed46O2z44YjcX8G8euUPdlmNBX0vK8EE8iXJudHMnEt6bjXM_k0', 1564151234537, 1564151234537),
(127, 'vivo 1817', '8.1.0', '3 (1.3)', 'de8019a5', 'eiNtrTANlWY:APA91bF6d2CWrJ3_WmOHzL2JvQpujpnhieUhQrXTvfoEJyvvjmlh3oqMlXND-ZcdxaaqwxYd8zo78nlWUL0BDxHH844zYrl60iHyq-6DkrITfPxfbr3lhO0eTWGIav5m1gkp6c-OuQ2s', 1564302060195, 1564302060195),
(128, 'samsung SM-J710GN', '8.1.0', '3 (1.3)', '5203b1b2e896636b', 'dKF2oKefw3w:APA91bEn_8UMtjJehNBfdlBa6KP93TG2HhJJe0tsExRi_pajWOnfjwxWNCqhVu0RVla0jXf0kXO56kBXe8FYQ4uAVvJz_ADDkYYkRh8Pcxdc-zWN-yJp98MDJ_T1FlusfV4fzP7grCVC', 1564432974802, 1564432974802),
(129, 'samsung SM-J710F', '8.1.0', '3 (1.3)', '52031ac1f4169401', 'cTBwMsF6L30:APA91bEBEX4XQR5sQn3nz7uSkiplOzltm3M8BEov8uuHFW1einE943H4tZteveMW468O5VKTKzQKmvNkshTYVCNzB31p1mv_kvbE23wsm2q_TelfKTo3tC4kpszocUA5h4MiQdBG2-Uw', 1564473710359, 1564473710359),
(130, 'samsung SM-A305F', '9', '3 (1.3)', 'RR8M30NMR6Z', 'ebdMK6Cw09g:APA91bFwrIABXXs_1bBCDosGqod4Sj60aUsixVmimlZM8zksSQZnmgx9HvcNBgQt4g2njJFM1b_XM5hRIbRPi_A6uwI6_6f2YebPy8FISBbqjg628kRZtGzO6ZyGjWm8iYqxlnYEBd6f', 1564560566863, 1564560566863),
(131, 'Xiaomi Redmi 4X', '7.1.2', '3 (1.3)', 'f403deca7ce4', 'fYGsTfobWws:APA91bHBr0Vw6bApECQQJXQ3WB8uBwnQNMnygfxZAF-ehVr8n4X7d5r6CKesGLrMj01br8ligKR_f88tfwz6r4n-HkdRoB1uSL1lcc8c2UGpFM_QsbPpGHNGXRO6lD-DO0sMOnix_tng', 1564843229979, 1564843229979),
(132, 'OPPO A37f', '5.1.1', '3 (1.3)', '787aad1', 'cc26O1NpGaU:APA91bH51MXccG30jRDJsZQx4rc2auGOfaFoR2PrsXoRq7S1pbaU29FosTzptsea6sMvoyJsg97e-uKtgcMhag_6TsGqTzMMWmrDjKMQBknFo3zBWnFn2wrJnPjN0DKYvMtqf-hc9qXr', 1564884341432, 1564884455225),
(133, 'samsung SM-G531H', '5.1.1', '3 (1.3)', '42008e64c4c3a200', 'd1QdTTLJSqI:APA91bH1XF9Sdf-hxtAgGcooc4UWoWhXV4Ib_LZZnyEFbSvFl6b5BfDRdOkL1Kp1VGwXibCAYbIGWmglUpXbEHrla1CZxEVYoEx6lhxuIppAlvdQ_7QDBT6Ld6vFocAt2EKAgKA-oqzo', 1565362095779, 1565362095779),
(134, 'Xiaomi 2014817', '4.4.4', '3 (1.3)', '14dcc865', 'fDrDqcVU3BE:APA91bGtpvcOMJyq0aNc24V64K7yI0kcxAV4s8FQ94vlHLymcGH81rJ7AbJGyqtdx1mpBsXSR82pcvxCaljwJoaf9AG9Fnuv0lQde63CGffGqNm0y7k3NAC8EbS2LaTwquUA1kKbAESu', 1565752156360, 1565752156360),
(135, 'samsung SM-C900F', '8.0.0', '3 (1.3)', 'a397fcc7', 'eMSRhItiUlM:APA91bE-BWbq9gLnEPM7tx3-uPFJc182C3R7rG1vsnOqq2GvOxI7EwBsTDAFzvJNGvLq6Aj6l_iYGnUb6G8XVXP8KbWHdvpX-SWir_6kBfNOk_289TqfbjY02fjFPwJrZCZzQKAtbraI', 1565965232565, 1565965232565),
(136, 'samsung SM-J111F', '5.1.1', '3 (1.3)', '42007e3aacdc6300', 'dR66jCx-o2M:APA91bGVaHRQdWtLolmKf5zi0Efp60fIJbcil8ks6qPVD2d8kNFlbnn7_IQ8SLmwIGfSI5KrnJXxa5nxujaurqXUZNocWOVV28SCEWYw9WpYcQT3ygVxKchc0GyxRl1A3z0efiYK6mBA', 1566066906441, 1566066906441),
(137, 'asus ASUS_X00ID', '8.1.0', '3 (1.3)', 'HBAXGF00Z9912NM', 'cZ2XKTFPlWU:APA91bGFmvtiRMWsxRw8W1UzZLhwiR7cP6_g4ZqcFoa_y9lyogSwiz-dr_8Z5Cd6Zd4u_pvese0-6TZJgYIK8hrDGWS1CAx5EvUH3c48yiYhhQDzMnk1VUDCoTx08M4WVCslujBaLImf', 1566136799427, 1566136799427),
(138, 'Xiaomi Redmi 4A', '7.1.2', '3 (1.3)', '5cec72d7d240', 'cBe2imQqKMQ:APA91bG96_7SB83kFyftDQXzAmBMTESGSt_u0nGGsGKMc0GJyETbmIrAYx3a6lCo7Qx0PW0rmb8AvXXi7kSNPdA0SseiHhxgv72y203gDdMCkmSxaT4tXkZ1qfMoI6WXIKrd8vksN9Ns', 1566330108756, 1566344942921),
(139, 'samsung SM-A510F', '7.0', '3 (1.3)', '3300f796a72d6307', 'cGXY43omL0s:APA91bHbHc7lnrIuSR2nJJN6eAT__qtD06bAphLG7oAPLdJtUk_rj8Oe3XtNffSi_Xqo5NTWqIHgNnzrgi6Zc0Tu-Q3lJsUjxXoTrPN4EFFOvJorOmmjOf4NQQKdvR9aRjnprGpbApAz', 1566384947076, 1566384947076),
(140, 'samsung SM-J320G', '5.1.1', '3 (1.3)', '42000d259bdb5300', 'dVwA-S0rzMw:APA91bEbnfRO4XEGWfNpoSJI91N0Dv7vR4OJGRg52xDriL46Ska0-Xaew2omPjYWTAJti94n3sFosfz061v09BT0QuPA6ncdHFUyNjghpvaRglLjtChc1Z-oOGxGy88v-45zLgTgwhPW', 1566392886789, 1566392886789),
(141, 'Xiaomi Redmi Note 5', '9', '3 (1.3)', '26f60a2', 'fYEmMv-QD5w:APA91bG1G9cbU4vD5e3F4ZSUi7e54tX5Gnb6QKDdqQDNZhlpqpjmdVYz9aCFXScREFhOF_pEzOjM3kOD3EiBgxax2iZQ4O8J0namtiCfx3H0qtDMCxjZVSEKD6blhK2pdRAQnbsrcrVc', 1566491785033, 1566491785033),
(142, 'vivo 1610', '6.0.1', '3 (1.3)', '21386c64', 'dD9gXmavgD8:APA91bE_6Ozv60gOj5mqOnbggJ1jxXLFNNHU4ibfSTpGBmJpiQTrLldk6LSjpEHbac-MepSCerlnNRYe94SNrC9HtA2T2j2z-hfqxzYDYfv1uMlcNfcYI0I3MU30qdwJ0UMA3h1FpS7N', 1566531777107, 1566531777107),
(143, 'OPPO CPH1609', '6.0', '3 (1.3)', 'ORIR4H4PZPS84SNB', 'fyq8fTYLRZ8:APA91bEwT0FFmgLWN9m17huM38dChAMf4vHUObhPQPpbnq6bfaRzIyg8E-NtmPFyKgQ4UpsswoMfsYFtOYDyw9nBM8OInk3mKVlRH8qTB8JnZaeXPQL_XQEBRaWt-1apPNWHzWE61RoM', 1566532488894, 1566532488894),
(144, 'OPPO CPH1823', '8.1.0', '3 (1.3)', 'KNCUNJRKZPBISO7S', 'fftrFWMyVzY:APA91bHdKtQ1t4ncjHSW3dmLSAxU0GO3EYJ7xD7C6q6GvZO4NryOWUCM4sSvziHDHVB0wtBy-F_OW0UjQ2az_FKkNfazWk4t6at3szh2ePeghTZIEztcgcm5Azd5U2p0PC7XRlF7WQTO', 1566552071313, 1566552071313),
(145, 'asus ASUS_X00TD', '9', '3 (1.3)', 'J6AXGF014973HPC', 'dmR0QjoPmNI:APA91bFrTGbuRkdY30G6MgObGTLw3T30OpmE7g38QJscdVWWReCiSj3By_hiTXgLh3FcIouSPwvyPLzOaruy3atqwJBb3SM19w_pGykoEUODpu_rfI0S1zUSv_M6CnF7JzkQ1EAs3GfO', 1566620863034, 1566620863034),
(146, 'samsung SM-J701F', '8.1.0', '3 (1.3)', '52005c78b88dc4f5', 'c4ca9dwHxQk:APA91bESPg-RxL-R2dzM4xbHGiAILlFE76wveFGNsxEogXyHgLgR6s2cQvtVbsXMuFLGVK_95Iml_T49OIoHl0S4Kcwu3iD_WvVJ7XygcfgAz0Bm2TbCpSQqYJVnS2KycwCMVgXJmFqE', 1566751703603, 1566751703603),
(147, 'Infinix X572-LTE', '8.1.0', '3 (1.3)', '027141079Q004299', 'dw6fJxjItcE:APA91bGbMGBNM7laLljOusnGGpgOlZO_HkJKuD27GuDUryZAPj61eATnPK5cwTN75UTHg8t-x3c-GxJeWyrKGoiAdlPOQrlgyJJA0cjxGcCCHKwo_EDC-SzrM9f0PF_0TnMZJ_tzmpug', 1566893271788, 1566893271788),
(148, 'OPPO CPH1969', '9', '3 (1.3)', 'O7KJYP5H5TCUVWEM', 'cYawYrOw5Uo:APA91bGyMdtYwXM7AlxqlE8CRj7NPsHIMr4qgtE3yorkzRnTjryr76GO1B0YywGk8DEZuTLQG-7CsUASqHhqSfM3l7vUvU7pqh2vhmbARK6Q-gvh1Mpnv2Fb5ZcMwuF-qsTdgF6m_ZJI', 1566971671280, 1566971671280),
(149, 'samsung SM-N935F', '9', '3 (1.3)', 'ce10171a80f61a2d01', 'ctEiaA43KvI:APA91bEhVZsffMWAf2oDs8n0eOnechhcz81KT5CwwJMSDyju6eoLMgrp_XSQmHn-C1VzybYZwVzzkaxTzzs3NL4vS5XH_E5_F4YltnmgiGy5Xn0rpl1ZRnrqvNp7ouScvG6T3tAaY3Mj', 1567063079729, 1567063079729),
(150, 'Xiaomi Redmi 4X', '6.0.1', '3 (1.3)', 'ae11d2817ce4', 'cPH_rOMvpDA:APA91bEuhsUcsJK9UL7F_1F-vGi1vev-IAJWal7enqBVeHrLYgbTIf5ttFECt4J2XuEONrk3bjAriH_i65SejQcM95whwehz-5miqjZ-jv3v2mKgeA2z899kvPyvgKvyOy3dsMJuKeW7', 1567211079450, 1567211079450),
(151, 'SPC L52 Pro', '7.0', '3 (1.3)', 'K36CX107918DO002559', 'eLmTSQjt3JE:APA91bGrQpt29ICv3DDEJTmfUkHoMj5pglNqp7jzQxtsA79LLA70bELkqDN6JC0SrOiBIgLbEMj0PqCKxP21_ldjvbnyXajdL_XQJzpaZSDY5t7GeNOovOHgbeO6pviQAdNM2Xg0TAEN', 1567425511834, 1567425511834),
(152, 'OPPO CPH1909', '8.1.0', '3 (1.3)', '9HFQU4H6S4RCVOTS', 'eiWKJYUw_1Q:APA91bFOzOjKmNdyaO-wpz-KaROrvrsXuZYYPTqTNQOboLu_1YPoQY0DgTgXfoViTSVV2hyYhd5O5oXeqAQBheAFx7Zqgk0beAlYEY4qu4SjQdgpyW-cpY5pZVrDA3b8j2J6v40jjWOk', 1567470577228, 1567470577228),
(153, 'samsung GT-P3100', '4.1.2', '3 (1.3)', 'c1607770789c711', 'cTRwWv7LA7w:APA91bGRbN-QYlF_IzFPcybaauVenkdc-jWc3VsXCtKHOvjV8AhP-HTCv0k1Dna8zPzMcyffMgze0VQXQvYtvm1vGZP_mkmCd9XEYKw8q8-ryAD5re5jsydNjZdggH3b7ldKPT8GyTTv', 1567523297524, 1567523297524),
(154, 'Xiaomi Redmi 3S', '6.0.1', '3 (1.3)', 'aed0387d53', 'cu1jQNJhXu0:APA91bEIpBuogGCcU4OsWvs33QiOBX93Dyytjko2aoKxsjpEk76BH8pnwmbaMq_LUl63ZOzlSAQ1CzL7gf65zrKdTmtAHVnLhQGmz3CixgtA7hHm9PknJBcsII5mvt2oQg9T3tOP9yhL', 1567845365155, 1567845365155),
(155, 'Xiaomi 2014813', '4.4.4', '3 (1.3)', '9964c2ca', 'fQzIGMVNU00:APA91bFIhAlsfCf7HKDdd5ZGPh5PfXcHT5vJbBD0-H-lU7a3y_y_hkcXF793dbyIZBOCzyL5LOSKd1mlDEce2ne5CzvaEsAReqyi_09hupkwHki53I7F9G_Q_RGCHAWmeiUFG2anaXYd', 1568009545129, 1568009545129),
(156, 'Xiaomi Redmi 5A', '7.1.2', '3 (1.3)', '2af405c57cf5', 'fuc1nyETorc:APA91bEiJD8E-KW98pqvYqGChAUwhXEjy4peKbVpLH15tLSJbJinfwvKga-_ZUaShfEXJ99FMZRtmExBMyuJHuKOGbnltS2Ai9HlKJaINb60msZYNtmB2AuhsGz5JWtjzB2h8vbhOxfc', 1568023354908, 1568023725787),
(157, 'vivo 1718', '7.1.2', '3 (1.3)', '5979244e', 'cnaSLNgo2wE:APA91bE-W74PkxUJVbbVmk4rpa5aWOxDKF7Oc4TCrGTaI05bLvI1kqTgFLxf75sFTtft1IkfVq79CyxqpYbBJj215Fli4QuH7PHFcbdXuS-n-Yq93IluBzbqN50ta3VNyyktEcVgnwxh', 1568040237712, 1568040237712),
(158, 'Realme RMX1941', '9', '3 (1.3)', '65MBTCSGIBSKBQYP', 'dlHVi863MOA:APA91bGIm5jvG26GY7qFaDEiJSU_GJZEPqU_PYAAgjxKvnfZ966f2u_avcTrn_-zxrQzodTzZAEU2oen506XRBaAhqBWhXyGCEQbS8Ewv9x7ToZN7nZubfxCp8x_OTOjJX6LQAbKacaV', 1568085176922, 1568085176922),
(159, 'Xiaomi Redmi Note 7', '9', '3 (1.3)', 'aee6bc1', 'dua3JbP6qwA:APA91bEGnucgKeL8Ckn1kUf_KBba4-A9JkRt3xicl9xmUnV_eU4m7p4b2zBuUwEbtu4Xbq3m1FfyMoCFIe4JvJxfIOSALA6qCii2CrHiSKuF9WglMEXCpT5zIUdhnhQ3pNwgcvBj0Emz', 1568094224547, 1568094224547),
(160, 'Xiaomi Mi A2', '9', '3 (1.3)', '9614d3d', 'dKMUe9_6pL0:APA91bHOijRsFdvdAju3lPoGJPBJYTLmxyoAzilWJJ11-7mCaqn9sMhh_El6xtFwvbjrprJyNfH9m1Vv1B9bf9yBtRBfx4ZxgtzggOi-030YYnba_yxr8zqfkqoMnQ-qP7UlTRQ6LqAk', 1568098798394, 1568098798394),
(161, 'samsung SM-G930S', '8.0.0', '3 (1.3)', 'ce081608f0f0272f05', 'etKFhvo87TA:APA91bFWpI5gdnQNF3cQT8fvKV7a9_X3sIq7sggQKhoNaJbdtO95ulsc-8JlJdHn_qndRwnpgi4cg2cfr_BRXJnTqcqRC4BdfrL5HUISWeyi4Hp1BfdjIOfVs-nRanN-NEdmKWAmeWw-', 1568205917302, 1568205917302),
(162, 'Xiaomi Redmi 3', '5.1.1', '3 (1.3)', '28e1d046', 'eDKHeR3c09A:APA91bEtAAqcAhhUV3Yf6SQyb2oW0QTGHyZcE7fG_Tl9GF2yQrJekcnqo99DD52y-hPB7ueIV5MmrJMVvfIHnIdl5miSmRP7fzv69JMWW-182DcYNuqRlQe0mTjKWwsBZ6jyuMQMmXXO', 1568206161565, 1568276778809),
(163, 'samsung SM-J730G', '9', '3 (1.3)', '52008293ea182561', 'f8dwWBE2CIY:APA91bENJ-FQjldbp2FetKN8rfl4PMUmFMbMRSU5l8e9i-30rSuJC7TLaeNUPWTNqOSH9L5CC6KZUAUo7TIZIKh3Y0wwpA9xW-LQb4HYEO5Ci2Pac8wQoUBL6nSR9lVSjLTjM3KhyzGX', 1568370838848, 1568370838848);

-- --------------------------------------------------------

--
-- Struktur dari tabel `news_info`
--

CREATE TABLE IF NOT EXISTS `news_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `brief_content` varchar(200) NOT NULL,
  `full_content` text NOT NULL,
  `image` varchar(110) NOT NULL,
  `draft` tinyint(1) NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `news_info`
--

INSERT INTO `news_info` (`id`, `title`, `brief_content`, `full_content`, `image`, `draft`, `status`, `created_at`, `last_update`) VALUES
(1, 'Available Best Interior Stuff, Browse and Discover Now for Your Room.', 'Quisque efficitur diam sit amet quam porttitor, at dictum arcu viverra. Vivamus dapibus ante nunc, non malesuada enim gravida in.', '<div>Aliquam sed lorem quis urna eleifend aliquam ac ut urna. Sed tempus nisl tellus, vel tempor velit rutrum ac. Ut facilisis eget augue quis pulvinar. Nam a felis eu tortor convallis dapibus efficitur vel turpis. Etiam hendrerit pulvinar leo ac venenatis. Donec non felis et massa semper convallis nec id augue. Proin sit amet mollis nisl. Sed vestibulum auctor imperdiet. Nam ut enim non lacus blandit rhoncus non at turpis. Donec vel arcu malesuada, vulputate dui a, varius leo. Proin volutpat libero ultricies est auctor, eget facilisis ligula accumsan. Sed quis sapien hendrerit, luctus augue nec, aliquam urna. Mauris vestibulum convallis malesuada. Sed et ex cursus, fringilla tellus ac, maximus ipsum.</div><div><br></div><div>Sed viverra arcu in neque pulvinar dignissim. Nunc euismod aliquam magna, non efficitur lectus vehicula quis. Suspendisse vitae consequat arcu, eget malesuada dui. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec quis imperdiet libero. Donec imperdiet quis lectus vitae dignissim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam pulvinar pretium ipsum, et faucibus libero aliquet et. Quisque efficitur diam sit amet quam porttitor, at dictum arcu viverra. Vivamus dapibus ante nunc, non malesuada enim gravida in. Cras bibendum eros vitae lacus efficitur, vel pretium est rutrum.</div><div><br></div><div>Praesent id efficitur risus. Nullam dui ligula, eleifend in convallis at, porta vitae massa. Curabitur sapien lorem, congue non enim non, rutrum gravida ipsum. Duis dignissim sapien in sem finibus ultrices id nec justo. Nulla hendrerit sed eros ac rhoncus. Nullam varius tellus id venenatis cursus. Cras commodo metus mauris, eget posuere sapien blandit non. Aenean fermentum ligula a libero suscipit dictum. Nulla dictum odio ut vulputate aliquam. Suspendisse potenti. Cras pellentesque vel felis sed dignissim. Proin aliquet orci tincidunt dui convallis, facilisis accumsan urna semper.</div>', 'Sed tempus nisl tellus vel tempor velit rutrum ac Ut facilisis eget augue quis pulvinar.png', 0, 'FEATURED', 1482250793274, 1485795107581),
(2, 'Tokoku Memberikan Kenyamanan dalam berbelanja Online', 'Tokoku Memberikan Kenyamanan dalam berbelanja Online,yuk segera belanja di tokoku :)', '<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque urna ut metus sagittis, vel congue lorem iaculis. Sed hendrerit mauris id tempor faucibus. In facilisis nibh vulputate ante porttitor, et venenatis arcu placerat. Vestibulum tempor nisi enim, eget consectetur ex accumsan fermentum. Mauris ipsum lacus, imperdiet eget purus vel, convallis euismod augue. In sed fringilla sem. Aenean ultricies ullamcorper euismod.</div><div><br></div><div>Cras non nunc est. Nunc quis dapibus lorem. Proin dignissim interdum interdum. In vitae aliquet odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non venenatis nunc, id molestie velit. Vestibulum sagittis dui at nibh lacinia faucibus. Curabitur auctor sem diam. Aenean eu libero eget lorem tincidunt eleifend. Integer in diam felis.</div><div><br></div><div>Aliquam sed lorem quis urna eleifend aliquam ac ut urna. Sed tempus nisl tellus, vel tempor velit rutrum ac. Ut facilisis eget augue quis pulvinar. Nam a felis eu tortor convallis dapibus efficitur vel turpis. Etiam hendrerit pulvinar leo ac venenatis. Donec non felis et massa semper convallis nec id augue. Proin sit amet mollis nisl. Sed vestibulum auctor imperdiet. Nam ut enim non lacus blandit rhoncus non at turpis. Donec vel arcu malesuada, vulputate dui a, varius leo. Proin volutpat libero ultricies est auctor, eget facilisis ligula accumsan. Sed quis sapien hendrerit, luctus augue nec, aliquam urna. Mauris vestibulum convallis malesuada. Sed et ex cursus, fringilla tellus ac, maximus ipsum.</div>', 'Tokoku Memberikan Kenyamanan dalam berbelanja Online.jpg', 0, 'FEATURED', 1485793148268, 1553241201534),
(10, 'Unique Women''s Accessories Trend that Will Popular Current Season update', 'Cras in dapibus ligula. Vestibulum elementum ante at sapien consectetur porta.', '<div>Cras in dapibus ligula. Vestibulum elementum ante at sapien consectetur porta. Nulla laoreet mauris a orci posuere eleifend. Duis dictum nisl scelerisque lectus tincidunt, id tristique lorem eleifend. Curabitur nibh risus, commodo in consequat eget, consectetur vel tellus. Suspendisse dignissim, lorem ut luctus congue, mi justo lobortis ex, non suscipit est est id urna. Ut eget tortor accumsan, consectetur justo sit amet, tincidunt enim.</div><div><br></div><div>Proin efficitur justo eget tempus vulputate. Nunc viverra felis tortor, a porttitor tortor vehicula dictum. Nam quis sapien nec tellus ultricies dictum. Praesent vulputate dolor sed nisl consectetur, vel varius dui suscipit. Maecenas vulputate ligula a sem sollicitudin viverra. In a libero viverra, eleifend nisi vitae, vehicula arcu. Donec sagittis sem sit amet nulla aliquam, a convallis risus ultricies. Praesent tristique magna odio, a dictum felis hendrerit quis.</div>', 'Unique Womens Accessories Trend that Will Popular Current Season.jpg', 0, 'NORMAL', 1485992959638, 1552992544299);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `image` varchar(110) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `price_discount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `minim_grosir` int(10) DEFAULT '0',
  `price_grosir` decimal(10,0) DEFAULT '0',
  `minim_grosir_clude` int(11) DEFAULT '0',
  `price_grosir_clude` decimal(10,0) DEFAULT '0',
  `stock` int(10) NOT NULL,
  `draft` tinyint(1) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_unique_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=344 ;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id`, `name`, `image`, `price`, `price_discount`, `minim_grosir`, `price_grosir`, `minim_grosir_clude`, `price_grosir_clude`, `stock`, `draft`, `description`, `status`, `created_at`, `last_update`) VALUES
(3, 'Derek Heart Juniors', 'Derek Heart Juniors.jpg', '23.00', '0.00', 0, '0', 0, '0', 90, 0, '<div>Sed viverra arcu in neque pulvinar dignissim. Nunc euismod aliquam magna, non efficitur lectus vehicula quis. Suspendisse vitae consequat arcu, eget malesuada dui. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec quis imperdiet libero. Donec imperdiet quis lectus vitae dignissim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam pulvinar pretium ipsum, et faucibus libero aliquet et. Quisque efficitur diam sit amet quam porttitor, at dictum arcu viverra. Vivamus dapibus ante nunc, non malesuada enim gravida in. Cras bibendum eros vitae lacus efficitur, vel pretium est rutrum.</div><div><br></div><div>Praesent id efficitur risus. Nullam dui ligula, eleifend in convallis at, porta vitae massa. Curabitur sapien lorem, congue non enim non, rutrum gravida ipsum. Duis dignissim sapien in sem finibus ultrices id nec justo. Nulla hendrerit sed eros ac rhoncus. Nullam varius tellus id venenatis cursus. Cras commodo metus mauris, eget posuere sapien blandit non. Aenean fermentum ligula a libero suscipit dictum. Nulla dictum odio ut vulputate aliquam. Suspendisse potenti. Cras pellentesque vel felis sed dignissim. Proin aliquet orci tincidunt dui convallis, facilisis accumsan urna semper.</div>', 'READY STOCK', 1485624280740, 1486541362705),
(20, 'NXY Soft Matte Lip Cream', 'NXY Soft Matte Lip Cream.jpg', '10.00', '0.00', 0, '0', 0, '0', 90, 0, '<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque urna ut metus sagittis, vel congue lorem iaculis. Sed hendrerit mauris id tempor faucibus. In facilisis nibh vulputate ante porttitor, et venenatis arcu placerat. Vestibulum tempor nisi enim, eget consectetur ex accumsan fermentum. Mauris ipsum lacus, imperdiet eget purus vel, convallis euismod augue. In sed fringilla sem. Aenean ultricies ullamcorper euismod.</div><div><br></div><div>Cras non nunc est. Nunc quis dapibus lorem. Proin dignissim interdum interdum. In vitae aliquet odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non venenatis nunc, id molestie velit. Vestibulum sagittis dui at nibh lacinia faucibus. Curabitur auctor sem diam. Aenean eu libero eget lorem tincidunt eleifend. Integer in diam felis.</div>', 'READY STOCK', 1485624280740, 1486541352723),
(49, 'Sofa Living Room Recliner Nordvalla Medium', 'Sofa Living Room Recliner Nordvalla Medium.jpg', '10.00', '0.00', 0, '0', 0, '0', 90, 0, '<div>Sed viverra arcu in neque pulvinar dignissim. Nunc euismod aliquam magna, non efficitur lectus vehicula quis. Suspendisse vitae consequat arcu, eget malesuada dui. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec quis imperdiet libero. Donec imperdiet quis lectus vitae dignissim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam pulvinar pretium ipsum, et faucibus libero aliquet et. Quisque efficitur diam sit amet quam porttitor, at dictum arcu viverra. Vivamus dapibus ante nunc, non malesuada enim gravida in. Cras bibendum eros vitae lacus efficitur, vel pretium est rutrum.</div><div><br></div><div>Praesent id efficitur risus. Nullam dui ligula, eleifend in convallis at, porta vitae massa. Curabitur sapien lorem, congue non enim non, rutrum gravida ipsum. Duis dignissim sapien in sem finibus ultrices id nec justo. Nulla hendrerit sed eros ac rhoncus. Nullam varius tellus id venenatis cursus. Cras commodo metus mauris, eget posuere sapien blandit non. Aenean fermentum ligula a libero suscipit dictum. Nulla dictum odio ut vulputate aliquam. Suspendisse potenti. Cras pellentesque vel felis sed dignissim. Proin aliquet orci tincidunt dui convallis, facilisis accumsan urna semper.</div>', 'READY STOCK', 1485624280740, 1486541338627),
(78, 'iPhone 6 - Black', 'iPhone 6  Black.jpg', '10.00', '0.00', 0, '0', 0, '0', 90, 0, '<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque urna ut metus sagittis, vel congue lorem iaculis. Sed hendrerit mauris id tempor faucibus. In facilisis nibh vulputate ante porttitor, et venenatis arcu placerat. Vestibulum tempor nisi enim, eget consectetur ex accumsan fermentum. Mauris ipsum lacus, imperdiet eget purus vel, convallis euismod augue. In sed fringilla sem. Aenean ultricies ullamcorper euismod.</div><div><br></div><div>Cras non nunc est. Nunc quis dapibus lorem. Proin dignissim interdum interdum. In vitae aliquet odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non venenatis nunc, id molestie velit. Vestibulum sagittis dui at nibh lacinia faucibus. Curabitur auctor sem diam. Aenean eu libero eget lorem tincidunt eleifend. Integer in diam felis.</div>', 'READY STOCK', 1485624280740, 1486541321906),
(133, 'GoodDay Cappuccino', 'GoodDay Cappuccino.jpg', '15000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Goodday Cappuccino satu renteng 10 sachet', 'READY STOCK', 1516720166045, 1516721969569),
(134, 'Aqua Gelas 240 ml Dus', 'Aqua Gelas Dus.jpg', '36000.00', '0.00', 0, '0', 0, '0', 999, 0, 'Aqua gelas per dus isi 48 pcs', 'READY STOCK', 1516785437622, 1516790515319),
(135, 'Aqua Tanggung 600 ml Dus', 'Aqua Tanggung 600 ml Dus.jpg', '51000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Aqua Tanggung 600 ml, isi per dus 24 pcs', 'READY STOCK', 1516791296688, 1517644667003),
(136, 'Aqua Besar 1500 ml Dus', 'Aqua Besar 1500 ml Dus.jpg', '51000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Aqua besar 1500 ml per dus isi 12 pcs', 'READY STOCK', 1516796798488, 1517644616997),
(137, 'Teh Kotak 200 ml Dus', 'Teh Kotak 200 ml Dus.jpg', '65000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Teh Kotak 200 ml per dus isi 24 pcs', 'READY STOCK', 1516798390605, 1517644565854),
(138, 'Gas Alpiji 5.5 kg', 'Gas Alpiji 55 kg.jpg', '85000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Gas Alpiji 5.5 kg warna pink, bright gas Pertamina', 'READY STOCK', 1516880613538, 1516880803510),
(139, 'Ayam Kremes Grobogan', 'Ayam Kremes Grobogan.jpg', '20000.00', '0.00', 0, '0', 0, '0', 989, 0, 'Ayam goreng kremes dengan sambal pedas dan kremesnya yang gurih mengundang selera disandingkan dengan lalapan segar.&nbsp;<div>Ayam goreng kremes Grobogan&nbsp;</div><div>Lokasi di Jl. Ahmad Yani</div><div><br></div><div><b>Tambah nasi 5.000 rupiah</b></div>', 'READY STOCK', 1516881692747, 1516882129342),
(140, 'Bear Brand Malt Tea', 'Bear Brand Malt Tea.jpg', '8500.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Bear Brand Gold Malt dan white Tea', 'READY STOCK', 1516894361223, 1516894884837),
(141, 'Teh Kotak', 'Teh Kotak.jpg', '3500.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Teh Kotak 200 ml per pcs', 'READY STOCK', 1516895360417, 1516895902031),
(142, 'Telur Ayam', 'Telur Ayam.jpg', '24000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Harga Telur ayam dalam 1/2 piring', 'READY STOCK', 1516906616319, 1517644487279),
(143, 'Gas Elpiji 12 kg', 'Gas Elpiji 12 kg.jpg', '180000.00', '0.00', 0, '0', 0, '0', 997, 0, 'Gas Elpiji Pertamina 12 kg, non tabung (isi ulang)&nbsp;<div>Bright Gas</div>', 'READY STOCK', 1516946744208, 1516946744208),
(144, 'Bimoli Special Botol 2 lt', 'Bimoli Special Botol 2 lt.jpg', '36000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Bimoli Spesial botol 2 lt<div>Diproses sempurna untuk hasil gorengan yang nikmat</div>', 'READY STOCK', 1516948481808, 1516948481808),
(145, 'Gula Pasir 1 kg', 'Gula Pasir 1 kg.jpg', '13000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Gula pasir lokal 1 kg', 'READY STOCK', 1516953226089, 1517644363664),
(146, 'Frisian Flag 123 Madu 800 Gr', 'Frisian Flag 123 Madu 800 Gr.jpg', '83000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Susu Bendera Frisian Flag 800 gr', 'READY STOCK', 1517050068552, 1517050068552),
(147, 'So klin Pewangi 900 ml', 'So klin Pewangi 900 ml.jpg', '13000.00', '0.00', 0, '0', 0, '0', 1000, 0, '<span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Dengan keharuman yang tahan lama.&nbsp;</span><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Mengharumkan sekaligus melembutkan pakaian.&nbsp;</span></div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Mencegah timbulnya bau apek pada pakaian.</span></div>', 'READY STOCK', 1517063013790, 1517063013790),
(148, 'Naida Air Mineral Gelas', 'Naida Air Mineral Gelas.jpg', '19000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Air Mineral Naida ekonomis dan bermutu<div>220 ml per dus 48 pcs</div>', 'READY STOCK', 1517065451551, 1517065451551),
(149, 'Hit Pome 500 ml', 'Hit Pome 500 ml.jpg', '28000.00', '0.00', 0, '0', 0, '0', 1000, 0, '<div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Hit&nbsp;</span><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">aerosol Pomegranate Natural Fragrance 500 ml</span></div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;"><br></span></div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">* Hit cairan pembasmi serangga&nbsp;</span><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">* Ampuh membasmi nyamuk &amp; kecoa&nbsp;</span></div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">* Dengan wangi aerosol Pomegranate sehingga aman digunakan&nbsp;</span></div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">* Tidak menggangu pernapasan</span></div>', 'READY STOCK', 1517066726483, 1517066726483),
(150, 'Mamipoko ED XL isi 7', 'Mamipoko ED XL isi 7.jpg', '24000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Mamipoko popok bayi/anak tipe celana XL<div>Extra Dry Isi 7 pcs</div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Celana popok ini terbuat dari bahan yang lembut&nbsp;</span></div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">seperti kain dan memiliki kemampuan menyerap super&nbsp;</span></div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">seperti popok untuk membantu agar&nbsp;</span></div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">kulit bayi tetap kering dan nyaman</span><br></div>', 'READY STOCK', 1517067337797, 1517067337797),
(151, 'Sunlight Lemon Reffil 400 ml', 'Sunlight Lemon Reffil 400 ml.jpg', '8500.00', '0.00', 0, '0', 0, '0', 1000, 0, '<div>Sunlight lemon reffil 400 ml</div><ul><li>Cairan pencuci piring</li><li>Diperkaya dengan ekstrak lemon</li><li>Membuat peralatan makan terbebas dari kotoran dan lemak yang menempel</li><li>Aman digunakan</li></ul>', 'READY STOCK', 1517067966084, 1517067966084),
(152, 'Lactogrow 3 Madu 750 gr', 'Lactogrow 3 Madu 750 gr.jpg', '98000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Susu anak 1 sampai 3 tahun rasa madu<div>Berat 750 gr</div>', 'READY STOCK', 1517111388687, 1517111388687),
(153, 'Miyako Dispenser panas Normal WD 190 H', 'Miyako Dispenser panas Normal WD 190 H.jpg', '200000.00', '0.00', 0, '0', 0, '0', 1000, 0, '<span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Dispenser Miyako WD_190</span><div><ul><li>big size - tabung stainless</li><li>350w</li><li>panas dan normal</li><li>panas hingga 90celcius<br></li></ul></div>', 'READY STOCK', 1517112285016, 1517119212256),
(154, 'Ojek dalam kota', 'Ojek dalam kota.jpg', '10000.00', '0.00', 0, '0', 0, '0', 9999, 0, 'Ojek dalam kota dan sekitarnya', 'READY STOCK', 1517119431563, 1519364731639),
(155, 'Ojek Antar Barang', 'Ojek Antar Barang.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ojek antar barang dalam kota dan sekitarnya', 'READY STOCK', 1517119565717, 1519364704638),
(156, 'Sampoerna Mild Merah', 'Sampoerna Mild Merah.jpg', '23000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Rokok Kretek Filter Sampoerna Mild<div>Isi 16 batang</div>', 'READY STOCK', 1517212195793, 1519131359874),
(157, 'Sampoerna Mild Hijau', 'Sampoerna Mild Hijau.jpg', '23000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Sampoerna Mild Hijau Mentol<div>Isi 16 batang</div>', 'READY STOCK', 1517236511444, 1519131383570),
(158, 'Marlboro Gold Light', 'Marlboro Gold Light.jpg', '25000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Rokok Marlboro Gold Light (putih) per bungkus isi 20 batang<br>', 'READY STOCK', 1517436251585, 1517436251585),
(159, 'Marlboro Merah', 'Marlboro Merah.jpg', '25000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Rokok Marlboro Merah per bungkus isi 20 batang<br>', 'READY STOCK', 1517436746867, 1517436746867),
(160, 'Marlboro Ice Blast', 'Marlboro Ice Blast.jpg', '27000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Marlboro Ice Blast Mentol per bungkus isi 20 batang<br>', 'READY STOCK', 1517437537332, 1519140755820),
(161, 'Marlboro Filter Black', 'Marlboro Filter Black.jpg', '25000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Marlboro Filter Black<br>Rokok Kretek Filter per bungkus isi 20 batang<br>', 'READY STOCK', 1517437668484, 1517437668484),
(162, 'U Mild', 'U Mild.jpg', '17000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Rokok U Mild Biru per bungkus isi 16 batang<br>', 'READY STOCK', 1517438085805, 1517438085805),
(163, 'Djarum Super Merah', 'Djarum Super Merah.jpg', '18000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Rokok Djarum Super kretek filter merah<br>per bungkus isi 12 batang<br>', 'READY STOCK', 1517438830205, 1517438830205),
(164, 'Djarum 76 Kretek', 'Djarum 76 Kretek.jpg', '16000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Rokok Djarum 76 kretek<br>per bungkus isi 12 batang<br>', 'READY STOCK', 1517439037781, 1517439037781),
(165, 'Djarum Black Mild', 'Djarum Black Mild.jpg', '18000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Rokok Djarum Black Mild (putih)<br>Per bungkus isi 16 batang<br>', 'READY STOCK', 1517439739532, 1517439739532),
(166, 'Djarum Black', 'Djarum Black.jpg', '18000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Rokok Djarum Black (hitam) per bungkus isi 16 batang<br>', 'READY STOCK', 1517440032125, 1517440032125),
(167, 'Djarum Black Menthol', 'Djarum Black Menthol.jpg', '18000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Rokok Djarum black menthol (hijau) per bungkus 16 batang<br>', 'READY STOCK', 1517440284362, 1517440284362),
(168, 'Santan Kara 65  ml', 'Santan Kara 65  ml.jpg', '3000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Santan kelapa siap pakai&nbsp;<div>Santan kara 65 ml</div>', 'READY STOCK', 1517568160622, 1517568160622),
(169, 'Sunco Minyak Goreng Refill 2 ltr', 'Sunco Minyak Goreng Refill 2 ltr.jpg', '31000.00', '0.00', 0, '0', 0, '0', 1000, 0, '<span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Minyak goreng diolah dari buah kelapa sawit yang segar. SunCo dibuat dengan teknologi mutakhir dengan melalui 5 tahapan proses</span>', 'READY STOCK', 1517568611193, 1517568611193),
(170, 'Aqua Galon Asli 19 ltr', 'Aqua Galon Asli 19 ltr.jpg', '35000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Aqua air mineral&nbsp; galon 19 liter, tidak termasuk galon<div>Jika tambah galon plus 50 rb rupiah</div>', 'READY STOCK', 1517578995774, 1517578995774),
(171, 'Indomilk Kids UHT 115 ml Vanilla', 'Indomilk Kids UHT 115 ml Vanilla.jpg', '2500.00', '0.00', 0, '0', 0, '0', 1000, 0, '<span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Susu UHT 115ml, - Susu siap minum dengan aneka , rasa. ( Vanilla )</span>', 'READY STOCK', 1517626024733, 1517626024733),
(172, 'Indomilk Kids UHT 115 ml Strawberry', 'Indomilk Kids UHT 115 ml Strawberry.jpg', '2500.00', '0.00', 0, '0', 0, '0', 1000, 0, '<span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Susu UHT 115ml, - Susu siap minum dengan aneka , rasa. ( Strawberry )</span>', 'READY STOCK', 1517626269769, 1517626269769),
(173, 'Indomilk Kids UHT 115 ml Coklat', 'Indomilk Kids UHT 115 ml Coklat.jpg', '2500.00', '0.00', 0, '0', 0, '0', 1000, 0, '<span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Susu UHT 115ml, - Susu siap minum dengan aneka , rasa. ( Coklat )</span>', 'READY STOCK', 1517626449806, 1517626449806),
(174, 'Indomilk Kids UHT 115 ml per Karton Vanilla', 'Indomilk Kids UHT 115 ml per Karton Vanilla.jpg', '90000.00', '0.00', 0, '0', 0, '0', 1000, 0, '<span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Susu UHT 115ml, - Susu siap minum</span><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">dengan aneka , rasa. ( Vanilla )</span></div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Per karton isi 40 pcs</span></div>', 'READY STOCK', 1517629726116, 1517629726116),
(175, 'Indomilk Kids UHT 115 ml per Karton Strawberry', 'Indomilk Kids UHT 115 ml per Karton Strawberry.jpg', '90000.00', '0.00', 0, '0', 0, '0', 1000, 0, '<span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Susu UHT 115ml, - Susu siap minum&nbsp;</span><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">d</span><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">engan aneka , rasa. ( Strawberry )</span></div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Per karton isi 40 pcs</span></div>', 'READY STOCK', 1517630046335, 1517630046335),
(176, 'Indomilk Kids UHT 115 ml per Karton Coklat', 'Indomilk Kids UHT 115 ml per Karton Coklat.jpg', '90000.00', '0.00', 0, '0', 0, '0', 1000, 0, '<span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Susu ULTRA 115ml, - Susu siap minum&nbsp;</span><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">dengan aneka , rasa. ( Coklat )</span></div><div><span style="color: rgb(60, 64, 67); font-family: Roboto-Regular, HelveticaNeue, Arial, sans-serif;">Per karton isi 40 pcs</span></div>', 'READY STOCK', 1517630280317, 1517630280317),
(177, 'Coca Cola 390 ml Pack', 'Coca Cola 390 ml Pack.jpg', '48000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Coca Cola 390 ml<br>per Pack isi 12 Botol<br>', 'READY STOCK', 1517675295435, 1517675295435),
(178, 'Fanta 390 ml Pack', 'Fanta 390 ml Pack.jpg', '48000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Fanta Merah 390 ml<br>per Pack isi 12 Botol<br>', 'READY STOCK', 1517675687105, 1517675687105),
(179, 'Sprite 390 ml Pack', 'Sprite 390 ml Pack.jpg', '48000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Sprite 390 ml<br>per Pack isi 12 Botol<br>', 'READY STOCK', 1517676919488, 1517676919488),
(180, 'Teh Pucuk Harum 350 ml Dus', 'Teh Pucuk Harum 350 ml Dus.jpg', '57000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Teh pucuk harum 350 ml<br>per Dus isi 24 Botol<br>', 'READY STOCK', 1517677447515, 1517677447515),
(181, 'Pulpy Orange 350 ml Pack', 'Pulpy Orange 350 ml Pack.jpg', '63000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Minute Maid Pulpy Orange 350 ml<br>per Pack isi 12 Botol<br>', 'READY STOCK', 1517677740105, 1517677740105),
(182, 'Pocari Sweat 350 ml Dus', 'Pocari Sweat 350 ml Dus.jpg', '118000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Pocari Sweat 350 ml<br>per Dus isi 24 Botol<br>', 'READY STOCK', 1517678061679, 1517678061679),
(183, 'Tebs Tea With Soda 500 ml Dus', 'Tebs Tea With Soda 500 ml Dus.jpg', '145000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Tebs Tea With Soda 500 ml<br>per Dus isi 24 Botol<br>', 'READY STOCK', 1517678977366, 1517678977366),
(184, 'Susu Bear Brand 189 ml Dus', 'Susu Bear Brand 189 ml Dus.jpg', '220000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Nestle Bear Brand/susu beruang<br>Susu Steril siap minum<br>per Dus isi 30 Kaleng<br>', 'READY STOCK', 1517679214288, 1517679214288),
(185, 'Nescafe Kaleng Original 240 ml Pack', 'Nescafe Kaleng Original 240 ml.jpg', '185000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Nescafe Kaleng Original 240 ml<br>per Dus isi 24 Kaleng<br>', 'READY STOCK', 1517680048416, 1517683867930),
(186, 'Bintang Zero 330 ml Dus', 'Bintang Zero 330 ml Dus.jpg', '165000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Bintang Zero Nol Alkohol<br>per Dus isi 24 Kaleng<br>', 'READY STOCK', 1517680258005, 1517680258005),
(187, 'Yakult 65 ml Pack', 'Yakult 65 ml Pack.jpg', '9000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Yakult 65 ml<br>Minuman Susu Fermentasi<br>per Pack isi 5 Botol<br>', 'READY STOCK', 1517681416179, 1517681416179),
(188, 'Zoda Air Soda 250 ml Dus', 'Zoda Air Soda 250 ml Dus.jpg', '98000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Zoda Air Soda Botol 250ml 24pcs', 'READY STOCK', 1517681625606, 1517681625606),
(189, 'Kopiko 78 C original 240 ml Dus', 'Kopiko 78 C original 240 ml Dus.jpg', '85000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Kopiko 78 C original 240 ml <br>isi per Dus 24 Botol<br>', 'READY STOCK', 1517681808491, 1517681808491),
(191, 'Lasegar Kaleng 320 ml Dus', 'Lasegar Kaleng 320 ml Dus.png', '106000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Lasegar Kaleng 320 ml<br>per Dus 24 Kaleng<br>', 'READY STOCK', 1517683587398, 1517683762014),
(192, 'Beras Mangkok Mas 25 kg', 'Beras Mangkok Mas 25.png', '315000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Beras super poles mangkok mas 25 kg<br>', 'READY STOCK', 1517730112229, 1517730571658),
(193, 'Beras Mangkok Mas 10 kg', 'Beras Mangkok Mas 10 kg.png', '128000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Beras super poles mangkok mas 10 kg<br>', 'READY STOCK', 1517730431033, 1517730431033),
(194, 'Beras Mangkok Mas 5 kg', 'Beras Mangkok Mas 5 kg.png', '67000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Beras super poles mangkok mas 5 kg<br>', 'READY STOCK', 1517730586748, 1517730586748),
(195, 'Beras Dua Lele 20 Kg', 'Beras Dua Lele 20 Kg.png', '265000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Beras Dua Lele<br>Beras Rojo Lele Kualitas Terbaik 20 Kg<br>', 'READY STOCK', 1517731820611, 1517731820611),
(196, 'Beras Dua Lele 10 Kg', 'Beras Dua Lele 10 Kg.png', '134000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Beras Dua Lele<br>Beras Rojo Lele Kualitas Terbaik 10 Kg<br>', 'READY STOCK', 1517731941839, 1517731941839),
(197, 'Beras Dua Lele 5 Kg', 'Beras Dua Lele 5 Kg.png', '69000.00', '0.00', 0, '0', 0, '0', 999, 0, 'Beras Dua Lele<br>Beras Rojo Lele Kualitas Terbaik 5 Kg<br>', 'READY STOCK', 1517732018677, 1517732018677),
(198, 'Beras Anggur Delanggu 10 Kg', 'Beras Anggur Delanggu 10 Kg.png', '97000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Beras Anggur Delanggu 10 Kg<br>', 'READY STOCK', 1517732416449, 1517732416449),
(199, 'Pop Ice Blender 1Pack', 'Pop Ice Blender 1Pack.jpg', '50000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Pop Ice es Blender<br>aneka varian rasa<br><br>silahkan tuliskan rasa yang diinginkan pada kolom komentar<br><br>harga 1 pack isi 5 renteng<br>', 'READY STOCK', 1518071719859, 1518071719859),
(200, 'Coca Cola 390 ml', 'Coca Cola 390 ml.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Coca Cola 390 ml<br>', 'READY STOCK', 1518073036975, 1518073036975),
(201, 'Fanta 390 ml', 'Fanta 390 ml.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Fanta 390 ml', 'READY STOCK', 1518073638063, 1518073638063),
(202, 'Sprite 390 ml', 'Sprite 390 ml.png', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sprite 390 ml', 'READY STOCK', 1518073699211, 1518073699211),
(203, 'Teh Pucuk Harum 350 ml', 'Teh Pucuk Harum 350 ml.jpg', '4000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Teh Pucuk Harum 350 ml', 'READY STOCK', 1518073933620, 1518073933620),
(204, 'Minute Maid Pulpy Orange 350 ml', 'Minute Maid Pulpy Orange 350 ml.jpg', '7000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Minute Maid Pulpy Orange 350 ml', 'READY STOCK', 1518074126175, 1518074126175),
(205, 'Pocari Sweat 350 ml', 'Pocari Sweat 350 ml.png', '6000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pocari Sweat 350 ml', 'READY STOCK', 1518074266961, 1518074600283),
(206, 'Tebs Tea With Soda 500 ml', 'Tebs Tea With Soda 500 ml.jpg', '7000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Tebs Tea With Soda 500 ml', 'READY STOCK', 1518074861096, 1518074861096),
(207, 'Susu Bear Brand 189 ml', 'Susu Bear Brand 189 ml.jpg', '8000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Susu Steril Bear Brand 189 ml', 'READY STOCK', 1518075016922, 1518075016922),
(208, 'Nescafe Kaleng 240 ml', 'Nescafe Kaleng 240 ml.jpg', '9000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Nescafe Kaleng 240 ml<br>dengan varian rasa original. mocca, latte dan black roast<br>', 'READY STOCK', 1518078621281, 1518078621281),
(209, 'Bintang Zero 330 ml', 'Bintang Zero 330 ml.jpg', '8000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Bintang Zero 330 ml<br>Carbonated Malt Beverage, zero alcohol<br>', 'READY STOCK', 1518078900003, 1518078900003),
(210, 'Zoda Air Soda 250 ml', 'Zoda Air Soda 250 ml.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Zoda Air Soda 250 ml<br>Air soda merupakan sejenis air yang dikarbonasikan<br>&nbsp;dan dibuat bersifat \nefervesen dengan <br>penambahan gas karbon dioksida di bawah tekanan', 'READY STOCK', 1518079136693, 1518079136693),
(211, 'Kopiko 78 C 240 ml', 'Kopiko 78 C 240 ml.jpg', '6000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Kopiko 78 C 240 ml<br>Variants : Coffee Latte, Caramel Frappe, Mocharetta Packing', 'READY STOCK', 1518079722873, 1518097718338),
(212, 'Pop Ice Blender White Coffee', 'Pop Ice Blender White Coffee.jpg', '10000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Pop Ice Blender White Coffee<br>jumlah 1 renteng 10 pcs<br>', 'READY STOCK', 1518082663800, 1518082663800),
(213, 'Pop Ice Blender Strawberry', 'Pop Ice Blender Strawberry.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender rasa Strawberry<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518082991777, 1518082991777),
(214, 'Pop Ice Blender Permen Karet', 'Pop Ice Blender Permen Karet.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender rasa Permen Karet<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518083095206, 1518083095206),
(215, 'Pop Ice Blender Melon', 'Pop Ice Blender Melon.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender rasa Melon<br>jumlah 1 renteng / 20 pcs<br>', 'READY STOCK', 1518083295100, 1518083295100),
(216, 'Pop Ice Blender Mangga', 'Pop Ice Blender Mangga.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender rasa Mangga<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518085242752, 1518085242752),
(217, 'Pop Ice Blender Kacang Hijau', 'Pop Ice Blender Kacang Hijau.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender Rasa Kacang Hijau<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518087695025, 1518087695025),
(218, 'Pop Ice Blender Grape', 'Pop Ice Blender Grape.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender Rasa Grape / Anggur<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518087861344, 1518087861344),
(219, 'Pop Ice Blender Durian', 'Pop Ice Blender Durian.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender rasa Durian<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518087946598, 1518087946598),
(220, 'Pop Ice Blender Coklat', 'Pop Ice Blender Coklat.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender Coklat<br>jumlah isi 1 renteng / 10 pcs<br>', 'READY STOCK', 1518088011818, 1518088011818),
(221, 'Pop Ice Blender Coffee Latte', 'Pop Ice Blender Coffee Latte.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender rasa Coffee Latte<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518091157924, 1518091157924),
(222, 'Pop Ice Blender Coffee Moccacino', 'Pop Ice Blender Coffee Moccacino.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender rasa Coffee Moccacino<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518093456989, 1518093456989),
(223, 'Pop Ice Blender Choco Cookies', 'Pop Ice Blender Choco Cookies.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender rasa Choco Cookies<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518093583488, 1518093583488),
(224, 'Pop Ice Blender Blueberry', 'Pop Ice Blender Blueberry.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender rasa Blueberry<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518093705998, 1518093705998),
(225, 'Pop Ice Blender Avocado', 'Pop Ice Blender Avocado.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Ice Blender rasa Avocado<br>jumlah 1 renteng / 10 pcs<br>', 'READY STOCK', 1518093787972, 1518093787972),
(226, 'Lasegar Kaleng 320 ml', 'Lasegar Kaleng 320 ml.jpg', '6000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Lasegar Kaleng 320 ml<br>variant rasa : <br>Leci, anggur, strawberry, jeruk nipis, jeruk, apel, melon dan jambu<br>tuliskan rasa yang diinginkan pada kolom komentar / atau random<br>', 'READY STOCK', 1518095323545, 1518095323545),
(227, 'Pop Mie Rasa Baso Dus', 'Pop Mie Rasa Baso Dus.jpg', '85000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Mie Rasa Baso<br>1 dus isi 24 pcs<br>', 'READY STOCK', 1518095587597, 1518095587597),
(228, 'Pop Mie Rasa Kari Ayam Dus', 'Pop Mie Rasa Kari Ayam Dus.jpg', '85000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Mie Rasa Kari Ayam<br>1 dus isi 24 pcs<br>', 'READY STOCK', 1518096824820, 1518096824820),
(229, 'Pop Mie Rasa Ayam Dus', 'Pop Mie Rasa Ayam Dus.jpg', '85000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Mie Rasa Ayam<br>1 dus isi 24 pcs<br>', 'READY STOCK', 1518096981816, 1518096981816),
(230, 'Pop Mie Rasa Baso', 'Pop Mie Rasa Baso.jpg', '4000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Mie Rasa Baso Sapi<br>Mie Instan seduh 3 menit jadi<br>', 'READY STOCK', 1518097141847, 1518097141847),
(231, 'Pop Mie Rasa Kari Ayam', 'Pop Mie Rasa Kari Ayam.jpg', '4000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Mie Rasa Kari Ayam<br>Mie Instan seduh 3 menit siap makan<br>', 'READY STOCK', 1518097434559, 1518097434559),
(232, 'Pop Mie Rasa Ayam', 'Pop Mie Rasa Ayam.jpg', '4000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Pop Mie Rasa Ayam<br>Mie Instan seduh 3 menit siap makan<br>', 'READY STOCK', 1518097510621, 1518097510621),
(233, 'Bawang Merah', 'Bawang Merah.jpg', '30000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Bawang Merah lokal per kg<br>', 'READY STOCK', 1518160914507, 1519739376218),
(234, 'Bawang Putih', 'Bawang Putih.jpg', '30000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Bawang putih lokal per kg<br>', 'READY STOCK', 1518161003983, 1519739354340),
(235, 'Mizone 500 ml Dus', 'Mizone 500 ml Dus.jpg', '40000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Mizone 500 ml<br>Variant : Activ, Lychee Lemon, Apple Guava, Orange Lime<br><br>Rasa yang diinginkan tulis pada kolom komentar<br>', 'READY STOCK', 1518161739862, 1518161739862),
(236, 'Mizone 500 ml', 'Mizone 500 ml.jpg', '4000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Mizone 500 ml', 'READY STOCK', 1518162665391, 1518162665391),
(237, 'Korek Api Gas TOKAI Box', 'Korek Api Gas TOKAI Box.jpg', '77000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Korek Api Gas TOKAI<br>&nbsp;1 Box isi 50 pcs<br>', 'READY STOCK', 1518163963751, 1518163963751),
(238, 'Korek Api Gas TOKAI', 'Korek Api Gas TOKAI.jpg', '2500.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Korek Api Gas TOKAI<br>', 'READY STOCK', 1518164087999, 1518164087999),
(239, 'Teh Bandulan Pekalongan', 'Teh Bandulan Pekalongan.jpg', '2500.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Teh tubruk Cap Bandulan<br>Segar<br>Wangi<br>Sedap<br>Harum<br>Asli Pekalongan<br>', 'READY STOCK', 1518164329185, 1518164329185),
(240, 'Indomie Goreng Dus', 'Indomie Goreng Dus.jpg', '85000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Indomie Goreng Mie Instan 1 dus isi 40 bungkus', 'READY STOCK', 1518165099531, 1518165099531),
(241, 'Indomie Goreng', 'Indomie Goreng.jpg', '2500.00', '0.00', 0, '0', 0, '0', 9993, 0, 'Indomie Goreng Mie Instan<br>', 'READY STOCK', 1518165201338, 1518165201338),
(242, 'Indomie Soto Banjar Dus', 'Indomie Soto Banjar Dus.jpg', '81000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Indomie Soto Banjar <br>per Dus isi 40 pcs<br>', 'READY STOCK', 1518195672922, 1518195672922),
(243, 'Indomie Soto Banjar', 'Indomie Soto Banjar.jpg', '2500.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Indomie Soto Banjar mie instan<br>', 'READY STOCK', 1518195795392, 1518195795392),
(244, 'Indomie Goreng Sambal Mentah Dus', 'Indomie Goreng Sambal Mentah Dus.jpg', '85000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Indomie Goreng Sambal Mentah<br>per Dus isi 40 pcs<br>', 'READY STOCK', 1518196677422, 1518196677422),
(245, 'Indomie Goreng Sambal Mentah', 'Indomie Goreng Sambal Mentah.jpg', '2500.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Indomie Goreng Sambal Mentah mie instan<br>', 'READY STOCK', 1518196808982, 1518196808982),
(246, 'Indomie Kari Ayam Dus', 'Indomie Kari Ayam Dus.jpg', '85000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Indomie Kari Ayam <br>per Dus isi 40 pcs<br>', 'READY STOCK', 1518202402307, 1518202402307),
(247, 'Indomie Kari Ayam', 'Indomie Kari Ayam.jpg', '2500.00', '0.00', 0, '0', 0, '0', 9999, 0, 'Indomie Kari Ayam Mie Instan<br>', 'READY STOCK', 1518203231310, 1518203231310),
(248, 'Pulsa Reguler All Operator 100 Ribu', 'Pulsa Reguler All Operator 100 Ribu.jpg', '105000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'PULSA ALL OPERATOR<br><br>Tersedia pulsa 100 Ribu dengan harga 105 Ribu<br>(Belum termasuk ongkir)<br><br>Ingin isi pulsa ?? Kami langsung datang kerumah anda<br>Bayar ditempat anda<br>', 'READY STOCK', 1518568812174, 1518586557282),
(249, 'Pulsa Listrik Pintar 100 Ribu', 'Pulsa Listrik Pintar 100 Ribu.jpg', '105000.00', '0.00', 0, '0', 0, '0', 10000, 0, '<b>PULSA LISTRIK PINTAR (listrik prabayar)</b><br><br>Tersedia pulsa 100 Ribu dengan harga 105 Ribu<br>(belum termasuk ongkir)<br><br>Anda pesan kami langsung kerumah anda<br><br>Bayar ditempat (COD)<br>', 'READY STOCK', 1518569045936, 1518586684035),
(250, 'Mie Sedap Goreng Dus', 'Mie Sedap Goreng Dus.jpg', '88000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Mie Sedap Goreng<br>per dus isi 40 pcs<br>', 'READY STOCK', 1518593795925, 1518593795925),
(251, 'Mie Sedap Goreng', 'Mie Sedap Goreng.jpg', '2500.00', '0.00', 0, '0', 0, '0', 9990, 0, 'Mie Sedap Goreng<br>', 'READY STOCK', 1518594074983, 1518594371238),
(252, 'Mie Sedap Soto Dus', 'Mie Sedap Soto Dus.jpg', '81000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Mie Sedap Soto<br>per dus isi 40 pcs<br>', 'READY STOCK', 1518594142754, 1518594142754),
(253, 'Mie Sedap Soto', 'Mie Sedap Soto.jpg', '25000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Mie Sedap Soto mie instan<br>', 'READY STOCK', 1518594386791, 1518594386791),
(254, 'Sariwangi 1.85 Gr TB 25', 'Sariwangi 185 Gr TB 25.jpg', '5000.00', '0.00', 0, '0', 0, '0', 9998, 0, 'Sariwangi Teh Celup Melati 1.85 Gr <br>Isi 25 kantung<br>', 'READY STOCK', 1518601970093, 1518601970093),
(255, 'Sariwangi 1.85 Gr TB 25 Dus', 'Sariwangi 185 Gr TB 25 Dus.jpg', '230000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sariwangi teh celup 1.85 Gr<br>isi 1 Dus 48 Box<br>per box 25 kantung<br>', 'READY STOCK', 1518602099527, 1518602099527),
(256, 'Dancow 3 Coklat 400 Gr', 'Dancow 3 Coklat 400 Gr.jpg', '46000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Dancow 3 Coklat 400 Gr<br><br>Untuk anak usia 3 s/d 5 Tahun<br>', 'READY STOCK', 1518700869445, 1518700869445),
(257, 'Lagtogen 4 Vanilla 350 Gr', 'Lagtogen 4 Vanilla 350 Gr.jpg', '45000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Lagtogen 4 Vanilla 350 Gr<br><br>Untuk anak usia 3 s/d 5 Tahun<br>', 'READY STOCK', 1518700971625, 1518700971625),
(258, 'Royco Ayam 9 Gr', 'Royco Ayam 9 Gr.jpg', '5000.00', '0.00', 0, '0', 0, '0', 9990, 0, 'Royco Ayam 9 Gr<br><br>1 Renteng isi 12 pcs<br>', 'READY STOCK', 1518701923096, 1518701923096),
(259, 'Royco Ayam 9 Gr Dus', 'Royco Ayam 9 Gr Dus.jpg', '215000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Royco Ayam 9 Gr<br><br>1 Dus isi 576 pcs / 48 renteng (lusin)<br>', 'READY STOCK', 1518702057020, 1518702057020),
(260, 'Santan Kara 65 ml Dus', 'Santan Kara 65 ml Dus.jpg', '88000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Santan Kara 65 ml<br>&nbsp;per dus isi 36 pcs<br>', 'READY STOCK', 1518702498406, 1518702498406),
(261, 'Kopi Kapal Api Special 165 gr', 'Kopi Kapal Api Special 165 gr.jpg', '11000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Kopi Kapal Api Special 165 gr<br><br>Kopi Bubuk murni<br>', 'READY STOCK', 1518703683504, 1518703683504),
(262, 'Kopi Kapal Api Special 165 gr Dus', 'Kopi Kapal Api Special 165 gr Dus.jpg', '212000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Kopi Kapal Api Special 165 gr<br><br>per dus 20 pcs<br>', 'READY STOCK', 1518703952458, 1518703952458),
(263, 'Indomilk Coklat Kaleng Dus', 'Indomilk Coklat Kaleng Dus.jpg', '425000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Indomilk Coklat Kaleng<br>per Dus isi 48 pcs<br>', 'READY STOCK', 1519099218163, 1519099218163),
(264, 'Indomilk Coklat Kaleng', 'Indomilk Coklat Kaleng.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Indomilk Coklat Kaleng<br>Susu kental manis cair dengan rasa coklat<br>', 'READY STOCK', 1519111936743, 1519111936743),
(265, 'Indomilk Putih Kaleng Dus', 'Indomilk Putih Kaleng Dus.jpg', '425000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Indomilk Putih Kaleng<br>Susu Kental Manis<br>per dus isi 48 pcs<br>', 'READY STOCK', 1519112066629, 1519112066629),
(266, 'Indomilk Putih Kaleng', 'Indomilk Putih Kaleng.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Indomilk Putih Kaleng<br>Susu kental manis cair<br>', 'READY STOCK', 1519112178606, 1519112178606),
(267, 'Yamato Sardines 155 Gr Dus', 'Yamato Sardines 155 Gr Dus.jpg', '270000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Yamato Sardines 155 Gr<br><br>Sarden saus tomat<br>per dus isi 50 pcs<br>', 'READY STOCK', 1519119480892, 1519119480892),
(268, 'Yamato Sardines 155 Gr', 'Yamato Sardines 155 Gr.png', '5500.00', '0.00', 0, '0', 0, '0', 9999, 0, 'Yamato Sardines 155 Gr<br><br>Sarden kaleng dengan saus tomat<br>', 'READY STOCK', 1519119938295, 1519119938295),
(269, 'Kopi Kapal Api Bubuk 380 Gr Dus', 'Kopi Kapal Api Bubuk 380 Gr Dus.jpg', '168000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Kopi Kapal Api Bubuk 380 Gr<br><br>per Dus isi 9 pcs<br>', 'READY STOCK', 1519121440449, 1519121440449),
(270, 'Kopi Kapal Api Bubuk 380 Gr', 'Kopi Kapal Api Bubuk 380 Gr.jpg', '20000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Kopi Kapal Api Bubuk 380 Gr<br><br>Kopi bubuk bungkus putih<br>', 'READY STOCK', 1519121526078, 1519121526078),
(271, 'Bakso Daging Sapi', 'Bakso Daging Sapi.jpg', '13000.00', '0.00', 0, '0', 0, '0', 9996, 0, 'Bakso Daging Sapi', 'READY STOCK', 1519124141450, 1519124141450),
(272, 'Mie Ayam', 'Mie Ayam.jpg', '10000.00', '0.00', 0, '0', 0, '0', 9993, 0, 'Mie ayam <br>pada warung :<br>1. Podo Tresno<br>2. Grobogan<br><br>tuliskan pesan pilihan warung yang diinginkan pada <br>kolom komentar<br>', 'READY STOCK', 1519124262653, 1519124262653),
(273, 'Es Cincau Asli Grobogan', 'Es Cincau Asli Grobogan.png', '5000.00', '0.00', 0, '0', 0, '0', 9997, 0, 'Es cincau asli dari daun cincau<br><br>nikmat dan menyegarkan<br>', 'READY STOCK', 1519124438968, 1519124438968),
(274, 'Red Bold', 'Red Bold.jpg', '15000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Rokok Red Bold<br><br>Rokok Kretek Filter<br>', 'READY STOCK', 1519130578333, 1519130578333),
(275, 'Red Bold Slop', 'Red Bold Slop.jpg', '140000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Red Bold 1 slop isi 10 bungkus<br>', 'READY STOCK', 1519130726589, 1519130726589),
(276, 'Sampoerna Mild Merah Slop', 'Sampoerna Mild Merah Slop.jpg', '197000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sampoerna Mild Merah <br>per Slop isi 10 bungkus<br>', 'READY STOCK', 1519133909464, 1519133909464),
(277, 'Sampoerna Mild Menthol Slop', 'Sampoerna Mild Menthol Slop.jpg', '197000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sampoerna Mild Menthol <br>per Slop isi 10 pcs<br>', 'READY STOCK', 1519134310486, 1519134310486),
(278, 'Sampoerna Avolution Menthol Slop', 'Sampoerna Avolution Menthol Slop.jpg', '223000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sampoerna Avolution Menthol <br>per Slop isi 10 pcs<br>', 'READY STOCK', 1519136095064, 1519136095064),
(279, 'Sampoerna Avolution Merah Slop', 'Sampoerna Avolution Merah Slop.jpg', '223000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sampoerna Avolution Merah <br>per Slop isi 10 pcs<br>', 'READY STOCK', 1519136206796, 1519136206796),
(280, 'Sampoerna Avolution Menthol', 'Sampoerna Avolution Menthol.png', '25000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sampoerna Avolution Menthol<br><br>Rokok kretek filter dengan sensasi dingin<br>', 'READY STOCK', 1519136284902, 1519136284902),
(281, 'Sampoerna Avolution Merah', 'Sampoerna Avolution Merah.jpg', '25000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sampoerna Avolution Merah<br><br>Rokok kretek filter<br>', 'READY STOCK', 1519136390809, 1519136390809),
(282, 'U Mild Slop', 'U Mild Slop.jpg', '158000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Rokok U Mild <br>per Slop isi 10 pcs<br>', 'READY STOCK', 1519137674005, 1519137674005),
(283, 'Marlboro Merah Slop', 'Marlboro Merah Slop.jpg', '234000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Marlboro Merah <br><br>per Slop isi 10 pcs<br>', 'READY STOCK', 1519139867254, 1519139867254),
(284, 'Marlboro Gold Light Slop', 'Marlboro Gold Light Slop.jpg', '234000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Marlboro Gold Light (putih)<br><br>per Slop isi 10 pcs<br>', 'READY STOCK', 1519140569498, 1519140569498),
(285, 'Marlboro Ice Blast Slop', 'Marlboro Ice Blast Slop.jpg', '258000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Marlboro Ice Blast <br><br>per Slop isi 10 pcs<br>', 'READY STOCK', 1519141901292, 1519141901292),
(286, 'Marlboro Filter Black Slop', 'Marlboro Filter Black Slop.png', '234000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Marlboro Filter Black <br><br>per Slop isi 10 pcs<br>', 'READY STOCK', 1519142906092, 1519142906092),
(287, 'Djarum 76 Kretek Slop', 'Djarum 76 Kretek Slop.jpg', '154000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Djarum 76 Kretek<br><br>per Slop 10 pcs<br>', 'READY STOCK', 1519226370436, 1519226370436),
(288, 'Djarum 76 Filter Gold Slop', 'Djarum 76 Filter Gold Slop.jpg', '175000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Djarum 76 Filter Gold <br>per Slop isi 10 pcs<br>', 'READY STOCK', 1519226565982, 1519226565982),
(289, 'Djarum 76 Filter Gold', 'Djarum 76 Filter Gold.jpg', '19000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Djarum 76 Filter Gold<br><br>Rokok kretek filter<br>', 'READY STOCK', 1519226658244, 1519226658244),
(290, 'Nasi Goreng Ayam', 'Nasi Goreng Ayam.jpg', '13000.00', '0.00', 0, '0', 0, '0', 9990, 0, 'Nasi Goreng Ayam<br><br>Tuliskan pada kolom komentar<br>jika menghendaki pada warung tertentu<br>', 'READY STOCK', 1519368500278, 1519368500278),
(291, 'Air Mineral Isi Ulang', 'Air Mineral Isi Ulang.jpg', '5000.00', '0.00', 0, '0', 0, '0', 9997, 0, '<b>Air Minum Air Mineral Isi Ulang</b><br><br>Syarat :<br>1. Galon yang digunakan galon Aqua<br>2. Kondisi baik dan tidak bocor<br>3. Tidak terdapat tulisan atau coretan/tanda<br><br>Karena kami mengirim menggunakan galon yang kami sediakan<br>sistem tukar galon dengan galon isi<br><br>Jika menghendaki di isi pada depo tertentu,<br>tuliskan pada kolom komentar<br><br>terima kasih<br>', 'READY STOCK', 1519438877709, 1519438877709),
(292, 'Materai 6000', 'Materai 6000.jpg', '6000.00', '0.00', 0, '0', 0, '0', 9986, 0, 'Materai 6000 PT.&nbsp; Pos Indonesia', 'READY STOCK', 1519440488330, 1519440488330),
(293, 'Gudang Garam 16', 'Gudang Garam 16.jpg', '21000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Rokok Gudang Garam 16<br>Rokok kretek filter<br>', 'READY STOCK', 1519527855083, 1519527855083),
(294, 'Gudang Garam 16 Slop', 'Gudang Garam 16 Slop.jpg', '194000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Rokok Gudang Garam 16<br><br>isi per slop 10 pcs<br>', 'READY STOCK', 1519527920663, 1519528002171),
(295, 'Gudang Garam Surya 12', 'Gudang Garam Surya 12.jpg', '16000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Gudang Garam Surya 12<br><br>rokok kretek filter<br>', 'READY STOCK', 1519529579774, 1519529579774),
(296, 'Gudang Garam Surya 12 Slop', 'Gudang Garam Surya 12 Slop.jpg', '146000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Gudang Garam Surya 12<br><br>per slop isi 10 bks<br>', 'READY STOCK', 1519529636353, 1519529636353),
(297, 'Tepung Beras Rose Brand 500 Gr', 'Tepung Beras Rose Brand 500 Gr.jpg', '6000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Tepung Beras Rose Brand 500 Gr<br><br>Tepung beras berkualitas super<br>', 'READY STOCK', 1519529868851, 1519529868851),
(298, 'Saus Sambal Ekstra Pedas Indofood 275 Ml', 'Saus Sambal Ekstra Pedas Indofood 275 Ml.jpg', '10000.00', '0.00', 0, '0', 0, '0', 9995, 0, 'Saus Sambal Ekstra Pedas Indofood<br>Dengan sambal asli<br>', 'READY STOCK', 1519555060203, 1555054213368),
(299, 'Le Minerale 1500 Ml', 'Le Minerale 1500 Ml.jpg', '6000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Le Minerale 1500 Ml<br><br>Air mineral berkualitas dari sumber mata air<br>yang diproses dengan teknologi tinggi<br>untuk hasil yang higienis, sehat dan bermutu<br>', 'READY STOCK', 1519556525354, 1519556525354),
(300, 'Anak Jagung', 'Anak Jagung.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Anak Jagung<br>dijual per 5.000<br><br>Harga sesuai pasar<br>', 'READY STOCK', 1519739727784, 1519739727784),
(301, 'Bawang Bombai', 'Bawang Bombai.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Bawang Bombai<br><br>pembelian per setengah kilo<br>harga sesuai pasar, harga kami merupakan contoh<br><br>perbedaan harga tanggungan pembeli<br>harap di mengerti<br>', 'READY STOCK', 1519741204708, 1519741204708),
(302, 'Bayam Sekop Daun Lebar', 'Bayam Sekop Daun Lebar.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sayur Bayam sekop daun lebar<br><br>dijual per ikat<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.<br>', 'READY STOCK', 1519741961566, 1519741961566),
(303, 'Bayam Cabut', 'Bayam Cabut.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sayur Bayam Cabut<br><br>dijual per ikat<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519742371937, 1519742371937),
(304, 'Bunga Kol', 'Bunga Kol.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Bunga Kol<br><br>Dijual per Buah (ukuran standar)<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519742601233, 1519742601233),
(305, 'Buncis', 'Buncis.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sayur Buncis<br><br>Dijual per 10.000<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519742734942, 1519742734942),
(306, 'Cabe Merah', 'Cabe Merah.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Cabe Merah (Cabe besar)<br><br>Dijual per 5.000<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519742819436, 1519742819436),
(307, 'Cabe Rawit', 'Cabe Rawit.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Cabe Rawit<br><br>Dijual per 5.000 (kantong)<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519742921044, 1519742921044),
(308, 'Daun Bawang', 'Daun Bawang.jpg', '5000.00', '0.00', 0, '0', 0, '0', 1000, 0, 'Daun Bawang<br><br>Dijual per 5.000 (ikat)<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743000160, 1519743000160),
(309, 'Jagung Sayur', 'Jagung Sayur.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Jagung Sayur<br><br>Dijual per 10.000<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743061166, 1519743061166),
(310, 'Jahe Lokal', 'Jahe Lokal.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Jahe lokal<br><br>Dijual per 5.000<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743236142, 1519743236142),
(311, 'Kacang Panjang', 'Kacang Panjang.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Kacang Panjang<br><br>Dijual per ikat<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743304912, 1519743304912),
(312, 'Kangkung', 'Kangkung.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sayur Kangkung<br><br>Dijual per ikat<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743356090, 1519743356090),
(313, 'Kencur', 'Kencur.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Kencur Lokal<br><br>Dijual per 5.000<br><br>Harga sesuai pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743402870, 1519743402870),
(314, 'Kentang', 'Kentang.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Kentang Lokal<br><br>Dijual per setengah kilo<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743509454, 1519743509454),
(315, 'Ketimun', 'Ketimun.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ketimun<br><br>Dijual per 5.000<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743601310, 1519743601310),
(316, 'Kol Gobes', 'Kol Gobes.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sayur Kol Gobes<br><br>Dijual per buah ukuran sedang<br>atau sesuai permintaan<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743656289, 1519743656289),
(317, 'Kunyit Kunir', 'Kunyit Kunir.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Kunyit atau kunir<br><br>Dijual per 5.000<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743800700, 1519743800700),
(318, 'Lengkuas', 'Lengkuas.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Lengkuas<br><br>Dijual per 5.000<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519743870153, 1519743870153);
INSERT INTO `product` (`id`, `name`, `image`, `price`, `price_discount`, `minim_grosir`, `price_grosir`, `minim_grosir_clude`, `price_grosir_clude`, `stock`, `draft`, `description`, `status`, `created_at`, `last_update`) VALUES
(319, 'Pare', 'Pare.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sayur Pare<br><br>Dijual per 10.000<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519744023442, 1519744023442),
(320, 'Sawi Hijau', 'Sawi Hijau.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sawi Hijau atau sawi asin<br><br>Dijual per ikat<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519744329662, 1519744329662),
(321, 'Sawi Putih', 'Sawi Putih.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sawi Putih<br><br>Dijual per buah<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519744608714, 1519744608714),
(322, 'Daun Seledri', 'Daun Seledri.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Daun Seledri<br><br>Dijual per ikat<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519744677837, 1519744677837),
(323, 'Serai', 'Serai.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Serai masak<br><br>Dijual per ikat<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519744784394, 1519744784394),
(324, 'Terong Hijau', 'Terong Hijau.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Terong Hijau<br><br>Dijual per 5.000<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519744843414, 1519744843414),
(325, 'Terong Kecil', 'Terong Kecil.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Terong Kecil<br><br>Dijual per 5.000 / plastik<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519744907124, 1519744907124),
(326, 'Terong Ungu', 'Terong Ungu.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Sayur Terong Ungu<br><br>Dijual per 5.000<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519745108879, 1519745108879),
(327, 'Wortel', 'Wortel.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Wortel<br><br>Dijual per 10.000<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519745167133, 1519745167133),
(328, 'Daging Sapi', 'Daging Sapi.jpg', '70000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Daging Sapi<br><br>Dijual per setengah Kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519745608034, 1519745608034),
(329, 'Ikan Nila', 'Ikan Nila.jpg', '45000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ikan Nila<br><br>Dijual per kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519745766557, 1519745766557),
(330, 'Ikan Mas', 'Ikan Mas.jpg', '45000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ikan Mas<br><br>Dijual per Kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519745951896, 1519745951896),
(331, 'Ikan Patin', 'Ikan Patin.jpg', '45000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ikan Patin<br><br>Dijual per Kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519746013793, 1519746013793),
(332, 'Ikan Bandeng', 'Ikan Bandeng.jpg', '45000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ikan Bandeng<br><br>Dijual per Kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519746071546, 1519746071546),
(333, 'Ikan Tongkol', 'Ikan Tongkol.jpg', '50000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ikan Tongkol<br><br>Dijual per Kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519746333965, 1519746333965),
(334, 'Ikan Kembung', 'Ikan Kembung.jpg', '40000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ikan Kembung<br><br>Dijual per Kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519746423495, 1519746423495),
(335, 'Ikan Bawal', 'Ikan Bawal.jpg', '50000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ikan Bawal<br><br>Dijual per Kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519746586854, 1519746586854),
(336, 'Udang', 'Udang.jpg', '40000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Udang<br><br>Dijual per Setengah Kilo<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519746938445, 1519746938445),
(337, 'Cumi Cumi', 'Cumi Cumi.jpg', '50000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Cumi Cumi<br><br>Dijual per Kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519747332752, 1519747332752),
(338, 'Ikan Layang', 'Ikan Layang.jpg', '45000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ikan Layang<br><br>Dijual per Kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519747421700, 1519747421700),
(339, 'Daging Ayam Potong', 'Daging Ayam Potong.jpg', '40000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Daging Ayam Potong<br><br>Dijual per Kilo Gram<br><br>Harga menyesuaikan pasar<br>perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519747738604, 1519747738604),
(340, 'Udang Rebon Kering', 'Udang Rebon Kering.jpg', '5000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Udang Rebon Kering<br><br>Dijual per 5.000<br><br>Harga menyesuaikan pasar<br>Perbedaan harga menjadi tanggungan pembeli.<br>', 'READY STOCK', 1519792801298, 1519792801298),
(341, 'Teri Medan', 'Teri Medan.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Teri Medan Kering<br><br>Dijual per 10.000<br><br>Harga menyesuaikan pasar<br>Perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519793045524, 1519793045524),
(342, 'Ikan Asin Peda', 'Ikan Asin Peda.jpg', '10000.00', '0.00', 0, '0', 0, '0', 10000, 0, 'Ikan Asin Peda<br><br>Dijual per 10.000<br><br>Harga menyesuaikan pasar<br>Perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519793119407, 1519793119407),
(343, 'Buah Naga Merah', 'Buah Naga Merah.jpg', '25000.00', '0.00', 10, '23000', 0, '0', 100, 0, 'Buah Naga Merah<br><br>Dijual per Kilo Gram / Per Buah<br><br>Harga menyesuaikan pasar<br>Perbedaan harga menjadi tanggungan pembeli.', 'READY STOCK', 1519793382632, 1567748161699);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `product_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  KEY `fk_product_category_1` (`product_id`),
  KEY `fk_product_category_2` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product_category`
--

INSERT INTO `product_category` (`product_id`, `category_id`) VALUES
(133, 3),
(134, 9),
(138, 4),
(139, 8),
(140, 3),
(141, 3),
(143, 4),
(144, 4),
(146, 6),
(147, 5),
(148, 9),
(149, 7),
(150, 6),
(151, 5),
(152, 6),
(153, 10),
(158, 13),
(159, 13),
(161, 13),
(162, 13),
(163, 13),
(164, 13),
(165, 13),
(166, 13),
(167, 13),
(168, 4),
(169, 4),
(170, 4),
(171, 6),
(172, 6),
(173, 6),
(174, 9),
(175, 9),
(176, 9),
(145, 4),
(142, 4),
(137, 9),
(136, 9),
(135, 9),
(177, 9),
(178, 9),
(179, 9),
(180, 9),
(181, 9),
(182, 9),
(183, 9),
(184, 9),
(186, 9),
(187, 3),
(188, 9),
(189, 9),
(191, 9),
(185, 9),
(193, 4),
(192, 4),
(194, 4),
(195, 4),
(196, 4),
(197, 4),
(198, 4),
(199, 9),
(200, 3),
(201, 3),
(202, 3),
(203, 3),
(204, 3),
(205, 3),
(206, 3),
(207, 3),
(208, 3),
(209, 3),
(210, 3),
(212, 3),
(213, 3),
(214, 3),
(215, 3),
(216, 3),
(217, 3),
(218, 3),
(219, 3),
(220, 3),
(221, 3),
(222, 3),
(223, 3),
(224, 3),
(225, 3),
(226, 3),
(227, 9),
(228, 9),
(229, 9),
(230, 3),
(231, 3),
(232, 3),
(211, 3),
(235, 9),
(236, 3),
(237, 9),
(238, 13),
(239, 4),
(240, 9),
(241, 4),
(242, 9),
(243, 4),
(244, 9),
(245, 4),
(246, 9),
(247, 4),
(248, 12),
(249, 12),
(250, 9),
(252, 9),
(251, 4),
(253, 4),
(254, 4),
(255, 9),
(256, 6),
(257, 6),
(258, 4),
(259, 9),
(260, 9),
(261, 4),
(262, 9),
(263, 9),
(264, 3),
(265, 9),
(266, 3),
(267, 9),
(268, 4),
(269, 9),
(270, 4),
(271, 8),
(272, 8),
(273, 8),
(274, 13),
(275, 9),
(156, 13),
(157, 13),
(276, 9),
(277, 9),
(278, 9),
(279, 9),
(280, 13),
(281, 13),
(282, 9),
(283, 9),
(284, 9),
(160, 13),
(285, 9),
(286, 9),
(287, 9),
(288, 9),
(289, 13),
(155, 12),
(154, 12),
(290, 8),
(291, 4),
(292, 13),
(293, 13),
(294, 9),
(295, 13),
(296, 9),
(297, 4),
(299, 3),
(234, 14),
(233, 14),
(300, 14),
(301, 14),
(302, 14),
(303, 14),
(304, 14),
(305, 14),
(306, 14),
(307, 14),
(308, 14),
(309, 14),
(310, 14),
(311, 14),
(312, 14),
(313, 14),
(314, 14),
(315, 14),
(316, 14),
(317, 14),
(318, 14),
(319, 14),
(320, 14),
(321, 14),
(322, 14),
(323, 14),
(324, 14),
(325, 14),
(326, 14),
(327, 14),
(328, 14),
(329, 14),
(330, 14),
(331, 14),
(332, 14),
(333, 14),
(334, 14),
(335, 14),
(336, 14),
(337, 14),
(338, 14),
(339, 14),
(340, 14),
(341, 14),
(342, 14),
(298, 4),
(343, 14);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_image`
--

CREATE TABLE IF NOT EXISTS `product_image` (
  `product_id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  KEY `fk_table_images` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product_image`
--

INSERT INTO `product_image` (`product_id`, `name`) VALUES
(133, 'GoodDay Cappuccino_01516722007863.jpg'),
(134, 'Aqua Gelas Dus_01516786083577.jpg'),
(138, 'Gas Alpiji 5.5 kg_01516880762258.jpg'),
(140, 'Bear Brand Malt Tea_01516894767718.jpg'),
(140, 'Bear Brand Malt Tea_11516895059054.jpg'),
(141, 'Teh Kotak_01516895928600.jpg'),
(143, 'Gas Elpiji 12 kg_01516947640880.jpg'),
(144, 'Bimoli Special Botol 2 lt_01516949165444.jpg'),
(146, 'Frisian Flag 123 Madu 800 Gr_01517050211535.jpg'),
(148, 'Naida Air Mineral Gelas_01517065753668.jpg'),
(149, 'Hit Pome 500 ml_01517067101784.jpg'),
(152, 'Lactogrow 3 Madu 750 gr_01517111659960.jpg'),
(153, 'Miyako Dispenser panas Normal WD 190 H_01517112552139.jpg'),
(163, 'Djarum Super Merah_01517438941070.jpg'),
(164, 'Djarum 76 Kretek_01517439178031.jpg'),
(170, 'Aqua Galon Asli 19 ltr_01517579328749.jpg'),
(171, 'Indomilk Kids UHT 115 ml Vanilla_01517626258333.jpg'),
(172, 'Indomilk Kids UHT 115 ml Strawberry_01517626430418.jpg'),
(173, 'Indomilk Kids UHT 115 ml Coklat_01517626532550.jpg'),
(145, 'Gula Pasir 1 kg_01516953292688.jpg'),
(142, 'Telur Ayam_01516906802861.jpg'),
(137, 'Teh Kotak 200 ml Dus_01516798555495.jpg'),
(136, 'Aqua Besar 1500 ml Dus_01516797025536.jpg'),
(135, 'Aqua Tanggung 600 ml Dus_01516791552920.jpg'),
(181, 'Pulpy Orange 350 ml Pack_01517677905769.jpg'),
(156, 'Sampoerna Mild Merah_01517212359992.jpg'),
(157, 'Sampoerna Mild Hijau_01517236614278.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_order`
--

CREATE TABLE IF NOT EXISTS `product_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `buyer` varchar(50) NOT NULL,
  `address` varchar(300) NOT NULL,
  `email` varchar(50) NOT NULL,
  `shipping` varchar(20) NOT NULL,
  `date_ship` bigint(20) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `ongkir` decimal(10,0) DEFAULT '0',
  `total_fees` decimal(12,2) NOT NULL,
  `tax` decimal(12,2) DEFAULT '0.00',
  `serial` varchar(100) DEFAULT NULL,
  `bank_asalID` int(11) DEFAULT NULL,
  `bank_sender` varchar(20) DEFAULT NULL,
  `bank_norek` varchar(30) DEFAULT NULL,
  `bank_atas_nama` varchar(50) DEFAULT NULL,
  `bukti_pembayaran` text,
  `date_payment` datetime DEFAULT NULL,
  `date_shipping` datetime DEFAULT '0000-00-00 00:00:00',
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=47 ;

--
-- Dumping data untuk tabel `product_order`
--

INSERT INTO `product_order` (`id`, `code`, `buyer`, `address`, `email`, `shipping`, `date_ship`, `phone`, `comment`, `status`, `ongkir`, `total_fees`, `tax`, `serial`, `bank_asalID`, `bank_sender`, `bank_norek`, `bank_atas_nama`, `bukti_pembayaran`, `date_payment`, `date_shipping`, `created_at`, `last_update`) VALUES
(36, 'FN87324BY', 'Muhammad Nur Hadi', 'jalan singa utara kalicari Semarang', 'nurhadimuhammad003@gmail.com', 'J&T', 1555099132998, '0876346436', 'cjhkjjkl', 'BAYAR', '9000', '59000.00', '0.00', '45489d92', 1, 'BNI', '069688585778', 'hadi', 'avatar21_12042019195942_1ec5b6cc-4379-401e-a21e-d47ace725bb7.png', '2019-04-12 19:59:42', '0000-00-00 00:00:00', 1555099141934, 1555099141934),
(37, 'WV61742FU', 'sasnz', 'jl. panjang', 'zjkzsks@gmail.com', 'JNE', 1555300292017, '08794283794', '', 'WAITING', '46000', '59000.00', '0.00', '7133dc9', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1555300302676, 1555300302676),
(38, 'BA92907PA', 'sasnz', 'jl. panjang', 'zjkzsks@gmail.com', 'J&T', 1555300457326, '08794283794', 'hlbobkbk', 'WAITING', '13000', '68000.00', '0.00', '7133dc9', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1555300464780, 1555300464780),
(39, 'IA69622ZE', 'Iqbal Rofindi', 'Sadewa 7', 'iqbalrofindi@gmail.com', 'JNE', 1555744901952, '087731102091', 'yg besar', 'WAITING', '8000', '13000.00', '0.00', 'ab8803d', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1555744907580, 1555744907580),
(40, 'SL09436KW', 'Iqbal Rofindi', 'Sadewa 7', 'iqbalrofindi@gmail.com', 'POS', 1555901167598, '087731102091', '', 'WAITING', '7000', '23500.00', '0.00', 'ab8803d', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1555901175013, 1555901175013),
(41, 'IA05788LZ', 'Mahardika Nugraha', 'warigalit II no 285', 'dika@can.web.id', 'JNE', 1556617992430, '082220128072', '', 'WAITING', '8000', '18000.00', '0.00', 'AIH6CA9S7LNFYDHI', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1556618010131, 1556618010131),
(42, 'AC06837DI', 'Mahardika Nugraha', 'warigalit II no 285', 'dika@can.web.id', 'JNE', 1557375909391, '082220128072', '', 'WAITING', '8000', '23000.00', '0.00', 'AIH6CA9S7LNFYDHI', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1557289521194, 1557289521194),
(43, 'DJ19985LL', 'ema', 'ngaliyan', 'emmay90@gmail.com', 'JNE', 1559193550717, '081326503604', 'yang asin', 'BAYAR', '8000', '38000.00', '0.00', '52005e39ec84543b', 1, 'BNI', '5362727', 'ema', 'avatar21_08052019052327_ba9e16a4-f23b-42cb-9f76-56f1d8e2c0071325512162694744003.jpg', '2019-05-08 05:23:27', '0000-00-00 00:00:00', 1557292762548, 1557292762548),
(44, 'TG62402BD', 'Mahardika Nugraha', 'warigalit II no 285', 'dika@can.web.id', 'JNE', 1557300446059, '082220128072', '', 'WAITING', '10000', '20000.00', '0.00', 'AIH6CA9S7LNFYDHI', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1557300455982, 1557300455982),
(45, 'KJ98646FB', 'wahyu', 'jl. pangeran jayajarta no. 12', 'joy.wahyu17@gmail.com', 'JNE', 1557922515143, '082311284368', '', 'WAITING', '13000', '58000.00', '0.00', 'PJFUPZC6BUV465KJ', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1557576938289, 1557576938289),
(46, 'JJ86094AJ', 'novita goi', 'desa Toto Utara', 'novitagoi75@gmail.com', 'TIKI', 1562596604997, '082339421657', '', 'WAITING', '61000', '71000.00', '0.00', 'FQGESGV48DGAZLAM', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1562596647434, 1562596647434);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_order_detail`
--

CREATE TABLE IF NOT EXISTS `product_order_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `price_item` decimal(12,2) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_table_orders_item` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=53 ;

--
-- Dumping data untuk tabel `product_order_detail`
--

INSERT INTO `product_order_detail` (`id`, `order_id`, `product_id`, `product_name`, `amount`, `price_item`, `created_at`, `last_update`) VALUES
(39, 36, 298, 'Saus Sambal Ekstra Pedas Indofood 275 Ml', 5, '10000.00', 1555099141935, 1555099141935),
(40, 37, 147, 'So klin Pewangi 900 ml', 1, '13000.00', 1555300302677, 1555300302677),
(41, 38, 338, 'Ikan Layang', 1, '45000.00', 1555300464780, 1555300464780),
(42, 38, 342, 'Ikan Asin Peda', 1, '10000.00', 1555300464781, 1555300464781),
(43, 39, 340, 'Udang Rebon Kering', 1, '5000.00', 1555744907580, 1555744907580),
(44, 40, 261, 'Kopi Kapal Api Special 165 gr', 1, '11000.00', 1555901175015, 1555901175015),
(45, 40, 268, 'Yamato Sardines 155 Gr', 1, '5500.00', 1555901175015, 1555901175015),
(46, 41, 342, 'Ikan Asin Peda', 1, '10000.00', 1556618010132, 1556618010132),
(47, 42, 340, 'Udang Rebon Kering', 1, '5000.00', 1557289521196, 1557289521196),
(48, 42, 342, 'Ikan Asin Peda', 1, '10000.00', 1557289521196, 1557289521196),
(49, 43, 342, 'Ikan Asin Peda', 3, '10000.00', 1557292762548, 1557292762548),
(50, 44, 342, 'Ikan Asin Peda', 1, '10000.00', 1557300455983, 1557300455983),
(51, 45, 338, 'Ikan Layang', 1, '45000.00', 1557576938290, 1557576938290),
(52, 46, 309, 'Jagung Sayur', 1, '10000.00', 1562596647437, 1562596647437);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `username` varchar(20) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_username` (`username`),
  UNIQUE KEY `unique_email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `email`, `password`) VALUES
(1, 'Administrator', 'administrator', 'admin@mail.com', '25d55ad283aa400af464c76d713c07ad'),
(2, 'herokushop admin', 'herokoshop', 'herokushop', '25d55ad283aa400af464c76d713c07ad');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `product_image`
--
ALTER TABLE `product_image`
  ADD CONSTRAINT `fk_table_images` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `product_order_detail`
--
ALTER TABLE `product_order_detail`
  ADD CONSTRAINT `fk_table_orders_item` FOREIGN KEY (`order_id`) REFERENCES `product_order` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
