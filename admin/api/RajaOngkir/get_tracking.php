<?php

if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$waybill	= $_POST['waybill'];
		$courier	= $_POST['courier'];
	}

$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => "waybill=$waybill&courier=$courier",
	CURLOPT_HTTPHEADER => array(
		"content-type: application/x-www-form-urlencoded",
		"key: d9cc3e0463ce8ea9546ea9b012d7aba6"
	),
));

$response_curl = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	// echo $response_curl;
	$json = json_decode($response_curl, true);
	if ($json['rajaongkir']['status']['code'] == 200) {
		$response['success']				= 1;
		$response['courier_name']			= $json['rajaongkir']['result']['summary']['courier_name'];
		$response['resi']					= $json['rajaongkir']['result']['details']['waybill_number'];
		$response['status_kurir']			= $json['rajaongkir']['result']['summary']['status'];
		$response['origin']					= $json['rajaongkir']['result']['details']['origin'];
		$response['origin_address']			= $json['rajaongkir']['result']['details']['shipper_address1'];
		$response['destination']			= $json['rajaongkir']['result']['details']['destination'];
		$response['destination_address']	= $json['rajaongkir']['result']['details']['receiver_address1'];
		$response['tracked_count']			= count($json['rajaongkir']['result']['manifest']);
		$response['manifest']				= $json['rajaongkir']['result']['manifest'];
		echo(json_encode($response));
		exit;
	} else {
		$response['success']				= 0;
		$response['code']					= $json['rajaongkir']['status']['code'];
		$response['description']			= $json['rajaongkir']['status']['description'];
		echo(json_encode($response));
		exit;
	}
}