<?php
error_reporting(0);

if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$originType			= $_POST['originType'];
		$origin				= $_POST['origin'];
		$destination		= $_POST['destination'];
		$destinationType	= $_POST['destinationType'];
		$weight				= $_POST['weight'];
		$courier			= $_POST['courier'];
	}

$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => "originType=$originType&origin=$origin&destination=$destination&destinationType=$destinationType&weight=$weight&courier=$courier",
	CURLOPT_HTTPHEADER => array(
		"content-type: application/x-www-form-urlencoded",
		"key: d9cc3e0463ce8ea9546ea9b012d7aba6"
	),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	echo $response;
}