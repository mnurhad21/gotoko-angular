angular.module('App').controller('AddBankController', function ($rootScope, $scope, $http, $mdToast, $mdDialog, $route, $timeout, request) {

	// define variable
	var self 		= $scope, root = $rootScope;
	var is_new 		= ( root.getCurBankId() == null );
	var original 	= null;
	var now 		= new Date().getTime();
	var dir 		= "/uploads/bank/";

	root.search_enable 		= false;
	root.toolbar_menu 		= null;
	root.pagetitle 			= (is_new) ? 'Add Bank' : 'Edit Bank';
	self.button_text 		= (is_new) ? 'SAVE' : 'UPDATE';
	self.submit_loading 	= false;
	self.img_bank 				= {};
	root.closeAndDisableSearch();
	
	/* check edit or add new*/
	if (is_new) {
		self.img_bank.valid = false;
		original = { nama_bank: "", img_bank: "", norek: "", atas_nama: "", draft: ""};
		self.bank = angular.copy(original);
	} else {
		self.img_bank.valid = true;
		request.getOneBank(root.getCurBankId()).then(function(resp){ 
			original = resp.data;
			self.bank = angular.copy(original);
		});
	}

	/* for selecting img_bank file */
	self.onFileSelect = function (files) {
		self.img_bank.valid = false;
        self.img_bank.file = files[0];
		if (root.constrainFilePng(files[0])) {
			self.img_bank.valid = true;
		}
		$mdToast.show($mdToast.simple().content("Selected file").position('bottom right'));
	};


	/* method for submit action */
	self.done_arr = [false, false, false];
	self.submit = function(c) {
		
		self.submit_loading = true;
		self.resp_submit    = null;		
		self.submit_done    = false;
		self.done_arr       = [false, false];
		
		if(is_new){ // create
			c.img_bank = now + root.getExtension(self.img_bank.file);
			request.insertOneBank(c).then(function(resp){
				self.resp_submit = resp;
				if(resp.status == 'success'){
					self.done_arr[0] = true;
					request.uploadFileToUrl(self.img_bank.file, dir, c.img_bank, "").then(function(){  // upload img_bank image
						self.done_arr[1] = true; 
					});
				}else{
					self.done_arr[0] = true;
					self.submit_done = true;
				}
			});
		} else {  // update
			c.last_update = now;
			var oldname = angular.copy(c.img_bank);
			c.img_bank = (self.img_bank.file != null) ? c.nama_bank.replace(/[^\w\s]/gi, '') + root.getExtension(self.img_bank.file) : c.img_bank;
			request.updateOneBank(c.id, c).then(function(resp){
				self.resp_submit = resp;
				if(resp.status == 'success'){
					self.done_arr[0] = true;
					if(self.img_bank.file != null){ // upload img_bank image
						request.uploadFileToUrl(self.img_bank.file, dir, c.img_bank, oldname).then(function(){ 
							self.done_arr[1] = true; 
						}); 
					} else { 
						self.done_arr[1] = true; 
					}
				} else {
					self.done_arr[0] = true;
					self.submit_done = true;
				}
			});
		}

	};
  
	/* Submit watch onFinish Checker */
	self.$watchCollection('done_arr', function(new_val, old_val) {
		if(self.submit_done || (new_val[0] && new_val[1])){
			loop_run = false;
			$timeout(function(){ // give delay for good UI
				if(self.resp_submit.status == 'success'){
				    root.showConfirmDialogSimple('', self.resp_submit.msg, function(){
				        window.location.href = '#bank';
				    });
				}else{
                    root.showInfoDialogSimple('', self.resp_submit.msg);
				}
				self.submit_loading = false;
			}, 1000);
		}
	});

	/* checker when all data ready to submit */
	self.isReadySubmit = function () {
		self.is_clean = angular.equals(original, self.category);
		if (is_new) {
			return (!self.is_clean && self.img_bank.valid);
		} else {
			if (self.img_bank.file != null) return (self.img_bank.valid);
			return (!self.is_clean);
		}
	};
	self.isImagesValid = function () {
		for (var i = 0; i < self.images.length; i++) {
			if (self.images[i].valid != null && !self.images[i].valid) return false;
		}
		return true;
	};


	/* for gcm notification */

	self.cancel = function () { window.location.href = '#bank'; };
	self.isNewEntry = function () { return is_new; };

	/* dialog View Image*/
	self.viewImage = function (ev, f) {
		$mdDialog.show({
			controller : ViewImageDialogController,
			parent: angular.element(document.body), targetEvent: ev, clickOutsideToClose: true, file_url: f,
			template: '<md-dialog ng-cloak aria-label="viewImage">' +
			'  <md-dialog-content style="max-width:800px;max-height:810px;" >' +
			'   <img style="margin: auto; max-width: 100%; max-height= 100%;" ng-src="{{file_url}}">' +
			'  </md-dialog-content>' +
			'</md-dialog>'
			
		})
	};

});

function ViewImageDialogController($scope, $mdDialog, $mdToast, file_url) {
	$scope.file_url = file_url;
}
